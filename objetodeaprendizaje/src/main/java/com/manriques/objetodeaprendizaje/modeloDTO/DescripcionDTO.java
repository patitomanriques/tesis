package com.manriques.objetodeaprendizaje.modeloDTO;

public class DescripcionDTO {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
