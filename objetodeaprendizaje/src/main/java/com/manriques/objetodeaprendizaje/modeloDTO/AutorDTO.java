package com.manriques.objetodeaprendizaje.modeloDTO;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AutorDTO {

    private String name;

    public AutorDTO(String name) {
        this.name = name;
    }

    public AutorDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
