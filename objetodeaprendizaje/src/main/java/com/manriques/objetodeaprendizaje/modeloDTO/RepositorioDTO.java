package com.manriques.objetodeaprendizaje.modeloDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositorioDTO {

    private String title;
    private Date updated;
    @JacksonXmlProperty(localName = "entry")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<ObjetoAprendizajeDTO> objetoAprendizajeDTOS;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public List<ObjetoAprendizajeDTO> getObjetoAprendizajeDTOS() {
        return objetoAprendizajeDTOS;
    }

    public void setObjetoAprendizajeDTOS(List<ObjetoAprendizajeDTO> objetoAprendizajeDTOS) {
        this.objetoAprendizajeDTOS = objetoAprendizajeDTOS;
    }
}
