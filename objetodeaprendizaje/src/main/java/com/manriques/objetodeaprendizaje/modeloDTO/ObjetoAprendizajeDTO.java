package com.manriques.objetodeaprendizaje.modeloDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "entry")
public class ObjetoAprendizajeDTO {

    private String title;
    private LinkDTO link;
    @JacksonXmlProperty(localName = "author")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<AutorDTO> author;
    private String id;
    private Date updated;
    private Date published;
    private String date;
    private String description;



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LinkDTO getLink() {
        return link;
    }

    public void setLink(LinkDTO link) {
        this.link = link;
    }

    public List<AutorDTO> getAuthor() {
        return author;
    }

    public void setAuthor(List<AutorDTO> author) {
        this.author = author;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
