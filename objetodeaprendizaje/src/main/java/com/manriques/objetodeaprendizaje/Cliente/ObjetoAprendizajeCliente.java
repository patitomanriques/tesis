package com.manriques.objetodeaprendizaje.Cliente;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.manriques.objetodeaprendizaje.modeloDTO.ObjetoAprendizajeDTO;
import com.manriques.objetodeaprendizaje.modeloDTO.RepositorioDTO;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ObjetoAprendizajeCliente {

    private WebTarget webTarget;
    private Client client;

    public ObjetoAprendizajeCliente() {
        client = new ResteasyClientBuilder().build();
    }

    public List<ObjetoAprendizajeDTO> obtenerObjetos(String url) throws IOException {
        try {
            webTarget = client.target(url);
            Response servicio = webTarget.request().get();
            RepositorioDTO response = new RepositorioDTO();
            if (servicio.getStatus() == 200) {
                String value = servicio.readEntity(String.class);
                XmlMapper mapper = new XmlMapper();
                response = mapper.readValue(value, RepositorioDTO.class);
            }
            return response.getObjetoAprendizajeDTOS();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }

    }
}
