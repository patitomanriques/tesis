package com.manriques.objetodeaprendizaje.mb;

import javax.faces.bean.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean(name = "validacionPermisos")
@SessionScoped
public class ValidacionPermisos {

    public ValidacionPermisos() {
    }


    private String usuario;
    private String password;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void validarUsuario(String usuario, String password) {
        if (usuario == null || usuario.isEmpty() || password == null || password.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "",
                    "Debe ingresar usuario y contraseña"));
        }
        FacesContext fccontext = FacesContext.getCurrentInstance();
        UsuarioController usuarioController = (UsuarioController) fccontext.getApplication().evaluateExpressionGet(fccontext, "#{usuarioController}", UsuarioController.class);
        if (usuarioController.existeUsuario(usuario, password)) {
            usuarioController.setearLogueado(usuario);
            if (!usuarioController.cambiaPassword(usuario)) {
                redireccion("/faces/index.xhtml");
            } else {
                redireccion("/faces/cambiarpassword.xhtml");
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "", "Usuario y/o contraseña incorrecta."));
            //redireccion("/faces/login.xhtml");
        }
    }

    public void cerrarSesion() {
        this.usuario = null;
        this.password = null;

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("#{usuarioController}", null);
        try {
            redireccion("/faces/login.xhtml");
            //return "/login.xhtml";
        } catch (Exception e) {
            e.printStackTrace();
            redireccion("/faces/login.xhtml");
            //return "/login.xhtml";
        }

    }

    public void redireccion(String url) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + url);
        } catch (IOException ex) {
            Logger.getLogger(ValidacionPermisos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
