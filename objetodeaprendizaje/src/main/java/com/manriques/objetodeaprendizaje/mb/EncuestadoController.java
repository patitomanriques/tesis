package com.manriques.objetodeaprendizaje.mb;

import com.manriques.objetodeaprendizaje.modelo.*;
import com.manriques.objetodeaprendizaje.mb.util.JsfUtil;
import com.manriques.objetodeaprendizaje.mb.util.PaginationHelper;
import com.manriques.objetodeaprendizaje.persistencia.AlumnoFacade;
import com.manriques.objetodeaprendizaje.persistencia.EncuestadoFacade;
import com.manriques.objetodeaprendizaje.persistencia.ExpertoFacade;
import com.manriques.objetodeaprendizaje.persistencia.UsuarioFacade;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("encuestadoController")
@SessionScoped
public class EncuestadoController implements Serializable {

    private Encuestado current;
    private EncuestadoWrapper currentEdit;
    private DataModel items = null;
    @EJB
    private com.manriques.objetodeaprendizaje.persistencia.EncuestadoFacade ejbFacade;
    @EJB
    private ExpertoFacade expertoFacade;
    @EJB
    private AlumnoFacade alumnoFacade;
    @EJB
    private UsuarioFacade usuarioFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<EncuestadoWrapper> objects = new ArrayList<>();
    private boolean esExperto;

    public EncuestadoController() {
    }

    public boolean isEsExperto() {
        return esExperto;
    }

    public void setEsExperto(boolean esExperto) {
        this.esExperto = esExperto;
    }

    public EncuestadoWrapper getCurrentEdit() {
        return currentEdit;
    }

    public void setCurrentEdit(EncuestadoWrapper currentEdit) {
        this.currentEdit = currentEdit;
    }

    public List<EncuestadoWrapper> getObjects() {
        if (objects == null) {
            this.objects = new ArrayList<>();
        }
        return objects;
    }

    public void setObjects(List<EncuestadoWrapper> objects) {
        this.objects = objects;
    }

    public EncuestadoFacade getEjbFacade() {
        return ejbFacade;
    }

    public Encuestado getSelected() {
        if (current == null) {
            current = new Encuestado() {
            };
            selectedItemIndex = -1;
        }
        return current;
    }

    private EncuestadoFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public void prepareList() {
        recreateModel();
        List<Experto> expertos = this.expertoFacade.findAll();
        List<Alumno> alumnos = this.alumnoFacade.findAll();
        if (!expertos.isEmpty()) {
            for (Experto experto : expertos) {
                Usuario usuario = this.usuarioFacade.usuarioPorIdEncuestado(experto.getId());
                EncuestadoWrapper encuestadoWrapper = new EncuestadoWrapper(experto.getId(), experto.getTipo(), experto.getApellido(), experto.getMail(),
                        experto.getEdad(), experto.getNombre(), usuario, "", "", experto.getEspecialidad());
                this.objects.add(encuestadoWrapper);
            }
        }
        if (!alumnos.isEmpty()) {
            for (Alumno alumno : alumnos) {
                Usuario usuario = this.usuarioFacade.usuarioPorIdEncuestado(alumno.getId());
                EncuestadoWrapper encuestadoWrapper = new EncuestadoWrapper(alumno.getId(), alumno.getTipo(), alumno.getApellido(), alumno.getMail(),
                        alumno.getEdad(), alumno.getNombre(), usuario, alumno.getCarrera(), alumno.getCurso(), "");
                this.objects.add(encuestadoWrapper);
            }
        }
        //items = new ArrayDataModel<Encuestado>((Encuestado[]) getFacade().findAll().toArray());
        this.redireccion("/faces/encuestado/List.xhtml");
    }

    public String prepareView() {
        current = (Encuestado) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Encuestado() {
        };
        selectedItemIndex = -1;
        return "Create";
    }

    public void create() {
        try {
            FacesContext contexto = FacesContext.getCurrentInstance();
            contexto.getExternalContext().getFlash().setKeepMessages(true);
            ExpertoController expertoController = (ExpertoController) contexto.getApplication().evaluateExpressionGet(contexto, "#{expertoController}", ExpertoController.class);
            AlumnoController alumnoController = (AlumnoController) contexto.getApplication().evaluateExpressionGet(contexto, "#{alumnoController}", AlumnoController.class);
            UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);
            if (this.currentEdit.getTipo().equals("Experto")) {
                Experto experto = new Experto();
                experto.setId(this.currentEdit.getId());
                experto.setTipo("Experto");
                experto.setUsuario(this.currentEdit.getUsuario());
                experto.setEdad(this.currentEdit.getEdad());
                experto.setMail(this.currentEdit.getMail());
                experto.setApellido(this.currentEdit.getApellido());
                experto.setNombre(this.currentEdit.getNombre());
                experto.setEspecialidad(this.currentEdit.getEspecialidad());
                Usuario usuario = this.currentEdit.getUsuario();
                usuario.setEncuestado(null);
                usuario.setEncuestado(experto);
                usuarioController.getEjbFacade().edit(usuario);
                //expertoController.crearExperto(experto);
            } else {
                Alumno alumno = new Alumno();
                alumno.setId(this.currentEdit.getId());
                alumno.setTipo("Alumno");
                alumno.setNombre(this.currentEdit.getNombre());
                alumno.setApellido(this.currentEdit.getApellido());
                alumno.setEdad(this.currentEdit.getEdad());
                alumno.setMail(this.currentEdit.getMail());
                alumno.setUsuario(this.currentEdit.getUsuario());
                alumno.setCurso(this.currentEdit.getCurso());
                alumno.setCarrera(this.currentEdit.getCarrera());
                Usuario usuario = this.currentEdit.getUsuario();
                usuario.setEncuestado(null);
                usuario.setEncuestado(alumno);
                usuarioController.getEjbFacade().edit(usuario);
                //alumnoController.crearAlumno(alumno);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Usuario actualizado correctamente"));
            this.redireccion("/faces/encuestado/Edit.xhtml");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public String prepareEdit() {
        //currentEdit = (EncuestadoWrapper) getItems().getRowData();
        //selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        if (currentEdit.getTipo().equals("Experto")) this.esExperto = true;
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EncuestadoUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Encuestado) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EncuestadoDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Encuestado getEncuestado(int id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Encuestado.class)
    public static class EncuestadoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EncuestadoController controller = (EncuestadoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "encuestadoController");
            return controller.getEncuestado(getKey(value));
        }

        int getKey(String value) {
            int key;
            key = Integer.parseInt(value);
            return key;
        }

        String getStringKey(int value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Encuestado) {
                Encuestado o = (Encuestado) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Encuestado.class.getName());
            }
        }

    }

    public void redireccion(String url) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + url);
        } catch (IOException ex) {
            Logger.getLogger(ValidacionPermisos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void habilitarCampos() {
        this.esExperto = this.currentEdit.getTipo().equalsIgnoreCase("Experto");

    }

}
