package com.manriques.objetodeaprendizaje.mb;

import com.manriques.objetodeaprendizaje.modelo.*;
import com.manriques.objetodeaprendizaje.mb.util.JsfUtil;
import com.manriques.objetodeaprendizaje.mb.util.PaginationHelper;
import com.manriques.objetodeaprendizaje.persistencia.MetricaFacade;
import com.manriques.objetodeaprendizaje.persistencia.ReporteEvaluacionFacade;
import com.manriques.objetodeaprendizaje.wrapper.DimensionWrapper;
import org.primefaces.model.chart.MeterGaugeChartModel;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("reporteEvaluacionController")
@SessionScoped
public class ReporteEvaluacionController implements Serializable {

    private ReporteEvaluacion current;
    private DataModel items = null;
    @EJB
    private com.manriques.objetodeaprendizaje.persistencia.ReporteEvaluacionFacade ejbFacade;

    @EJB
    private MetricaFacade metricaFacade;

    private PaginationHelper pagination;
    private int selectedItemIndex;
    //private String peso;
    private List<DimensionWrapper> dimensionesWrapper = new ArrayList<>();
    private double resultadoIndiceDimension = 0.0;
    private MeterGaugeChartModel meterGaugeModel1 = new MeterGaugeChartModel();
    private boolean calculoListo = false;
    private float indiceTotalDimensiones = 0;
    private List<ReporteEvaluacion> reporteEvaluacions = new ArrayList<>();
    private ReporteEvaluacion reporteVisualizar;
    private Date desde;
    private Date hasta;

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    List<Number> intervals = new ArrayList<Number>() {
        {
            add(0);
            add(0.30);
            add(0.60);
            add(1);
        }
    };

    public ReporteEvaluacion getReporteVisualizar() {
        return reporteVisualizar;
    }

    public void setReporteVisualizar(ReporteEvaluacion reporteVisualizar) {
        this.reporteVisualizar = reporteVisualizar;
    }

    public ReporteEvaluacionController() {
    }

    public List<ReporteEvaluacion> getReporteEvaluacions() {
        return reporteEvaluacions;
    }

    public void setReporteEvaluacions(List<ReporteEvaluacion> reporteEvaluacions) {
        this.reporteEvaluacions = reporteEvaluacions;
    }

    public float getIndiceTotalDimensiones() {
        return indiceTotalDimensiones;
    }

    public void setIndiceTotalDimensiones(float indiceTotalDimensiones) {
        this.indiceTotalDimensiones = indiceTotalDimensiones;
    }

    public boolean isCalculoListo() {
        return calculoListo;
    }

    public void setCalculoListo(boolean calculoListo) {
        this.calculoListo = calculoListo;
    }

    public MeterGaugeChartModel getMeterGaugeModel1() {
        return meterGaugeModel1;
    }

    public void setMeterGaugeModel1(MeterGaugeChartModel meterGaugeModel1) {
        this.meterGaugeModel1 = meterGaugeModel1;
    }

    public double getResultadoIndiceDimension() {
        return resultadoIndiceDimension;
    }

    public void setResultadoIndiceDimension(double resultadoIndiceDimension) {
        this.resultadoIndiceDimension = resultadoIndiceDimension;
    }

    public List<DimensionWrapper> getDimensionesWrapper() {
        return dimensionesWrapper;
    }

    public void setDimensionesWrapper(List<DimensionWrapper> dimensionesWrapper) {
        this.dimensionesWrapper = dimensionesWrapper;
    }

    public ReporteEvaluacion getSelected() {
        if (current == null) {
            current = new ReporteEvaluacion();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ReporteEvaluacionFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public void prepareList() {
        recreateModel();
        FacesContext contexto = FacesContext.getCurrentInstance();
        UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);
        if (usuarioController.getLogueado().getRol().getDescripcion().equals("Administrador")) {
            this.reporteEvaluacions = ejbFacade.findAll();
        } else {
            this.reporteEvaluacions = ejbFacade.reportesPorUsuario(usuarioController.getLogueado().getId());
        }
        redireccion("/faces/reporteEvaluacion/List.xhtml");
    }

    public String prepareView() {
        current = (ReporteEvaluacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public void prepareCreate() {
        dimensionesWrapper = new ArrayList<>();
        meterGaugeModel1 = new MeterGaugeChartModel();
        indiceTotalDimensiones = 0;
        current = new ReporteEvaluacion();
        selectedItemIndex = -1;
        redireccion("/faces/reporteEvaluacion/Create.xhtml");

    }

    public void create() {
        try {
            FacesContext contexto = FacesContext.getCurrentInstance();
            UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);
            current.setUsuario(usuarioController.getLogueado());
            current.setValordecalidad(this.indiceTotalDimensiones);
            current.setFecha(new Date());
            List<Encuesta> encuestas = this.ejbFacade.consultarEncuestasPorUsuarioYObjeto(usuarioController.getLogueado().getId(), current.getObjetoAprendizaje().getId());

            current.setCantidadencuestas(encuestas.size());

            List<ReporteDimension> reporteDimensions = new ArrayList<>();
            for (DimensionWrapper dimensionWrapper : this.dimensionesWrapper) {
                ReporteDimension reporteDimension = new ReporteDimension();
                reporteDimension.setDimension(dimensionWrapper.getDimension());
                reporteDimension.setPeso(dimensionWrapper.getPeso());
                reporteDimension.setReporteEvaluacion(current);
                reporteDimensions.add(reporteDimension);
            }

            current.setReporteDimensions(reporteDimensions);

            getFacade().edit(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Reporte guardado correctamente"));
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error al guardar reporte"));
        }
    }


    public String prepareEdit() {
        current = (ReporteEvaluacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }


    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ReporteEvaluacionUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (ReporteEvaluacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ReporteEvaluacionDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public ReporteEvaluacion getReporteEvaluacion(int id) {
        return ejbFacade.find(id);
    }


    @FacesConverter(forClass = ReporteEvaluacion.class)
    public static class ReporteEvaluacionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ReporteEvaluacionController controller = (ReporteEvaluacionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "reporteEvaluacionController");
            return controller.getReporteEvaluacion(getKey(value));
        }

        int getKey(String value) {
            int key;
            key = Integer.parseInt(value);
            return key;
        }

        String getStringKey(int value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ReporteEvaluacion) {
                ReporteEvaluacion o = (ReporteEvaluacion) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + ReporteEvaluacion.class.getName());
            }
        }

    }

    public void estadisticas() {
        this.dimensionesWrapper = new ArrayList<>();
        FacesContext contexto = FacesContext.getCurrentInstance();
        PruebaController pruebaController = (PruebaController) contexto.getApplication().evaluateExpressionGet(contexto, "#{pruebaController}", PruebaController.class);
        UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);
        List<Metrica> metricas = this.metricaFacade.consultarMetricaPorCapa(this.current.getCapa().getId());
        double acumulador = 0;
        double resultadoIndiceDimension = 0;
        int k = 0;
        for (Metrica me : metricas) {
            double resultado = pruebaController.reporteEvaluacion(me.getDimension().getId(), this.current.getCapa().getId(), me.getId(), usuarioController.getLogueado().getId(), this.current.getObjetoAprendizaje().getId());
            if (resultado != 0) {
                k++;
                acumulador += resultado != -1 ? resultado : 0;
                if (!this.contieneDimension(me.getDimension())) {
                    DimensionWrapper dimensionWrapper = new DimensionWrapper(me.getDimension(), 0);
                    dimensionesWrapper.add(dimensionWrapper);
                }

            }

        }
        //resultadoIndiceDimension = (acumulador / k) * Double.parseDouble(this.peso);
        this.resultadoIndiceDimension = (acumulador / k) * 1;
        this.meterGaugeModel1 = new MeterGaugeChartModel(0, intervals);
        this.meterGaugeModel1.setTitle("Gráfico de importancia de calidad");
        this.meterGaugeModel1.setSeriesColors("66cc66,cc6666,E7E658,93b75f");
        this.meterGaugeModel1.setGaugeLabel("Importancia");
        this.meterGaugeModel1.setGaugeLabelPosition("bottom");

    }

    private boolean contieneDimension(Dimension dimension) {
        for (DimensionWrapper dw : this.dimensionesWrapper) {
            if (dw.getDimension().getId() == dimension.getId()) return true;
        }
        return false;
    }

    public void indiceTotalDimensiones() {
        //DecimalFormat df = new DecimalFormat("#.00");
        double sumatoria = 0f;
        double sumatoriaVerificacion = 0f;
        for (DimensionWrapper dimension : this.dimensionesWrapper) {
            sumatoriaVerificacion += dimension.getPeso();
            sumatoria += (dimension.getPeso() * this.resultadoIndiceDimension);
        }
        if (sumatoriaVerificacion > 1 || sumatoriaVerificacion == 0) {
            sumatoria = 0f;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "El peso total asignado debe ser igual a 1"));
        }
        BigDecimal bd = BigDecimal.valueOf(sumatoria);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        this.indiceTotalDimensiones = bd.floatValue();
        this.meterGaugeModel1.setValue(sumatoria);
    }

    public void redireccion(String url) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + url);
        } catch (IOException ex) {
            Logger.getLogger(ValidacionPermisos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void visualizar() {
        this.meterGaugeModel1 = new MeterGaugeChartModel();
        current = this.reporteVisualizar;
        this.dimensionesWrapper = new ArrayList<>();
        this.indiceTotalDimensiones = current.getValordecalidad();

        List<Encuesta> encuestas = this.ejbFacade.consultarEncuestasPorUsuarioYObjeto(this.current.getUsuario().getId(), this.current.getObjetoAprendizaje().getId());
        Collections.sort(encuestas, new ObjectSortComparator());
        this.desde = encuestas.get(0).getFechacreacion();
        this.hasta = encuestas.get(encuestas.size() - 1).getFechacreacion();

        for (ReporteDimension rd : this.current.getReporteDimensions()) {
            DimensionWrapper d = new DimensionWrapper(rd.getDimension(), rd.getPeso());
            this.dimensionesWrapper.add(d);
        }

        this.meterGaugeModel1 = new MeterGaugeChartModel(0, intervals);
        this.meterGaugeModel1.setTitle("Gráfico de importancia de calidad");
        this.meterGaugeModel1.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
        this.meterGaugeModel1.setGaugeLabel("Importancia");
        this.meterGaugeModel1.setGaugeLabelPosition("bottom");
        this.meterGaugeModel1.setValue(current.getValordecalidad());


        redireccion("/faces/reporteEvaluacion/Visualizar.xhtml");
    }


}
