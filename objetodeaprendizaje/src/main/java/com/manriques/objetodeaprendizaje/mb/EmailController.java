package com.manriques.objetodeaprendizaje.mb;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailController {

    private EmailController() {
    }


    public static boolean enviarInvitacionPorEmail(String[] emails, String usuario) {
        try {
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            // TLS si está disponible
            props.setProperty("mail.smtp.starttls.enable", "true");
            // Puerto de gmail para envio de correos
            props.setProperty("mail.smtp.port", "587");
            // Nombre del usuario
            props.setProperty("mail.smtp.user", "Hades el piola");
            // Si requiere o no usuario y password para conectarse.
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props);
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress("tesis.oa.undec@gmail.com"));
            for (String email : emails) {
                message.addRecipients(Message.RecipientType.TO, email);
            }

            message.setSubject("Nueva Encuesta");

            message.setContent(generarHtmlInvitacion(usuario), "text/html; charset=utf-8");
            Transport t = session.getTransport("smtp");
            t.connect("tesis.oa.undec@gmail.com", "15416784p");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }

    }

    public static boolean enviarPasswordPorEmail(String email, String password) {
        try {
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            // TLS si está disponible
            props.setProperty("mail.smtp.starttls.enable", "true");
            // Puerto de gmail para envio de correos
            props.setProperty("mail.smtp.port", "587");
            // Nombre del usuario
            props.setProperty("mail.smtp.user", "Hades el piola");
            // Si requiere o no usuario y password para conectarse.
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props);
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress("tesis.oa.undec@gmail.com"));

            message.addRecipients(Message.RecipientType.TO, email);


            message.setSubject("Recuperar contraseña");

            message.setContent(generarHtmlPassword(password), "text/html; charset=utf-8");
            Transport t = session.getTransport("smtp");
            t.connect("tesis.oa.undec@gmail.com", "15416784p");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }

    }


    private static String generarHtmlPassword(String password) {
        return "<!DOCTYPE html> \n" +
                "                               <html xmlns=http://www.w3.org/1999/xhtml lang=es> \n" +
                "                                  <head> \n" +
                "                                    <meta http-equiv=Content-Type content=text/html; charset=UTF-8 /> \n" +
                "                                    <title>Codigo de verificacion</title> \n" +
                "                                    <meta name=viewport content=width=device-width, initial-scale=1.0/> \n" +
                "                                    <style> \n" +
                "                                         * { \n" +
                "                                              box-sizing: border-box; \n" +
                "                                            } \n" +
                "                                            \n" +
                "                                            body { \n" +
                "                                              background-color: #fafafa; \n" +
                "                                              display: -webkit-box; \n" +
                "                                              display: flex; \n" +
                "                                              -webkit-box-pack: center; \n" +
                "                                                      justify-content: center; \n" +
                "                                              -webkit-box-align: center; \n" +
                "                                                      align-items: center;\n" +
                "                                            } \n" +
                "                                    </style> \n" +
                "                                  </head> \n" +
                "                                  <body> \n" +
                "                                  <table style= \"width: 40vw;  border-radius: 40px;  overflow: hidden;  box-shadow: 0px 7px 22px 0px rgba(0, 0, 0, 0.1);\"> \n" +
                "                                       <tr> \n" +
                "                                            <th style=\"background-color: #24769a;width: 100%;  height: 60px;\" > \n" +
                "                                                <p><img src=\"https://i.ibb.co/vZJ0Dck/logo-sistema.png\" alt=SEMCOA style=\"padding: 0.25rem; background-color: #fff;border: 1px solid #dee2e6; border-radius: 0.25rem; max-width: 20%; height: auto;\"/></p>\n" +
                "                                             </th> \n" +
                "                                       </tr> \n" +
                "                                       <tr> \n" +
                "                                         <td style=\"width: 100%; display: -webkit-box; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; flex-direction: column; justify-content: space-around; -webkit-box-align: center; align-items: center; flex-wrap: wrap; padding: 15px;\"> \n" +
                "                                           <p style=\"font-size: 20px; text-align: center; color: #343434; margin-top: 0; font-family: 'Open Sans';\"> \n" +
                "                                            Utilice esta contraseña para ingresar al sistema: \n" +
                "                                           </p>            \n" +
                "                                         </td> \n" +
                "                                       </tr> \n" +
                "                                       <tr> \n" +
                "                                         <td style=\"display: block; width: 60%; margin: 30px auto; background-color: #24769a; border-radius: 40px; padding: 20px; text-align: center; font-size: 36px; font-family: 'Open Sans'; letter-spacing: 10px; box-shadow: 0px 7px 22px 0px rgba(0, 0, 0, 0.1);\"> \n" +
                "                                          <span style=\"color: #fffffF\" > " + password + " </span> \n" +
                "                                        </td> \n" +
                "                                       </tr> \n" +
                "                                  </table> \n" +
                "                                 </body> \n" +
                "                               </html>";
    }

    private static String generarHtmlInvitacion(String usuario) {
        return "<!DOCTYPE html> \n" +
                "                               <html xmlns=http://www.w3.org/1999/xhtml lang=es> \n" +
                "                                  <head> \n" +
                "                                    <meta http-equiv=Content-Type content=text/html; charset=UTF-8 /> \n" +
                "                                    <title>Codigo de verificacion</title> \n" +
                "                                    <meta name=viewport content=width=device-width, initial-scale=1.0/> \n" +
                "                                    <style> \n" +
                "                                         * { \n" +
                "                                              box-sizing: border-box; \n" +
                "                                            } \n" +
                "                                            \n" +
                "                                            body { \n" +
                "                                              background-color: #fafafa; \n" +
                "                                              display: -webkit-box; \n" +
                "                                              display: flex; \n" +
                "                                              -webkit-box-pack: center; \n" +
                "                                                      justify-content: center; \n" +
                "                                              -webkit-box-align: center; \n" +
                "                                                      align-items: center;\n" +
                "                                            } \n" +
                "                                    </style> \n" +
                "                                  </head> \n" +
                "                                  <body> \n" +
                "                                  <table style= \"width: 40vw;  border-radius: 40px;  overflow: hidden;  box-shadow: 0px 7px 22px 0px rgba(0, 0, 0, 0.1);\"> \n" +
                "                                       <tr> \n" +
                "                                            <th style=\"background-color: #24769a;width: 100%;  height: 60px;\" > \n" +
                "                                                <p><img src=\"https://i.ibb.co/vZJ0Dck/logo-sistema.png\" alt=SEMCOA style=\"padding: 0.25rem; background-color: #fff;border: 1px solid #dee2e6; border-radius: 0.25rem; max-width: 20%; height: auto;\"/></p>\n" +
                "                                             </th> \n" +
                "                                       </tr> \n" +
                "                                       <tr> \n" +
                "                                         <td style=\"width: 100%; display: -webkit-box; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; flex-direction: column; justify-content: space-around; -webkit-box-align: center; align-items: center; flex-wrap: wrap; padding: 15px;\"> \n" +
                "                                           <p style=\"font-size: 20px; text-align: center; color: #343434; margin-top: 0; font-family: 'Open Sans';\"> \n" +
                "                                            Usted fue seleccionado por " + usuario + " responder una encuesta: \n" +
                "                                           </p>            \n" +
                "                                         </td> \n" +
                "                                       </tr> \n" +
                "                                       <tr> \n" +
                "                                         <td style=\"display: block; width: 60%; margin: 30px auto; background-color: #24769a; border-radius: 40px; padding: 20px; text-align: center; font-size: 36px; font-family: 'Open Sans'; letter-spacing: 10px; box-shadow: 0px 7px 22px 0px rgba(0, 0, 0, 0.1);\"> \n" +
                "                                          <a style=\"color: #fffffF\" href=\"http://localhost:8080/objetoaprendizaje\">  Responder  </span> \n" +
                "                                        </td> \n" +
                "                                       </tr> \n" +
                "                                       <tr> \n" +
                "                                        <td> \n" +
                "                                          <p style=\"font-size: 20px; text-align: center; color: #343434; margin-top: 0; font-family: 'Open Sans';  font-style: italic; opacity: 0.3; margin-bottom: 0;\">Haga click en \"Responder\" para ingresar al sistema</p>\n" +
                "                                        </td> \n" +
                "                                       </tr> \n" +
                "                                  </table> \n" +
                "                                 </body> \n" +
                "                               </html>";
    }

}
