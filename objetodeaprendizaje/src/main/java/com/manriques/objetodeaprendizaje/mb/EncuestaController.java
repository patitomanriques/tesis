package com.manriques.objetodeaprendizaje.mb;

import com.manriques.objetodeaprendizaje.modelo.*;
import com.manriques.objetodeaprendizaje.mb.util.JsfUtil;
import com.manriques.objetodeaprendizaje.mb.util.PaginationHelper;
import com.manriques.objetodeaprendizaje.modeloDTO.AutorDTO;
import com.manriques.objetodeaprendizaje.modeloDTO.ObjetoAprendizajeDTO;
import com.manriques.objetodeaprendizaje.persistencia.EncuestaFacade;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

import com.manriques.objetodeaprendizaje.persistencia.ObjetoAprendizajeFacade;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DualListModel;

@Named("encuestaController")
@SessionScoped
public class EncuestaController implements Serializable {

    private Encuesta current;
    private DataModel items = null;
    @EJB
    private com.manriques.objetodeaprendizaje.persistencia.EncuestaFacade ejbFacade;
    @EJB
    private com.manriques.objetodeaprendizaje.persistencia.DimensionFacade ejbFacadeDimension;
    @EJB
    private ObjetoAprendizajeFacade ejbObjetoAprendizaje;

    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String capa = "";
    private DualListModel<Dimension> dimension = new DualListModel<>();
    private ObjetoAprendizajeDTO objetoSeleccionado;
    private List<Encuesta> pendientes = new ArrayList<>();
    private List<Encuesta> enviadas = new ArrayList<>();
    private List<Encuesta> misEncuestas = new ArrayList<>();

    public List<Encuesta> getMisEncuestas() {
        return misEncuestas;
    }

    public void setMisEncuestas(List<Encuesta> misEncuestas) {
        this.misEncuestas = misEncuestas;
    }

    public List<Encuesta> getPendientes() {
        return pendientes;
    }

    public void setPendientes(List<Encuesta> pendientes) {
        this.pendientes = pendientes;
    }

    public List<Encuesta> getEnviadas() {
        return enviadas;
    }

    public void setEnviadas(List<Encuesta> enviadas) {
        this.enviadas = enviadas;
    }

    public DualListModel<Dimension> getDimension() {
        return dimension;
    }

    public void setDimension(DualListModel<Dimension> dimension) {
        this.dimension = dimension;
    }

    public ObjetoAprendizajeDTO getObjetoSeleccionado() {
        return objetoSeleccionado;
    }

    public void setObjetoSeleccionado(ObjetoAprendizajeDTO objetoSeleccionado) {
        this.objetoSeleccionado = objetoSeleccionado;
    }

    public void seleccionCapa() {
        try {
            List<Dimension> dimensiones;
            if (capa.equals("Usuario")) {
                dimensiones = this.ejbFacadeDimension.buscardimensionesporcapa(2);
                dimension = new DualListModel<Dimension>(dimensiones, new ArrayList<Dimension>());
            }
            if (capa.equals("Experto")) {
                dimensiones = this.ejbFacadeDimension.buscardimensionesporcapa(1);
                dimension = new DualListModel<Dimension>(dimensiones, new ArrayList<Dimension>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String getCapa() {
        return capa;
    }

    public void setCapa(String capa) {
        this.capa = capa;
    }


    public EncuestaController() {
    }

    public Encuesta getSelected() {
        if (current == null) {
            current = new Encuesta();
            selectedItemIndex = -1;
        }
        return current;
    }

    public void setCurrent(Encuesta current) {
        this.current = current;
    }


    private EncuestaFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public void prepareList() {
        recreateModel();
        FacesContext contexto = FacesContext.getCurrentInstance();
        UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);
        if (usuarioController.getLogueado().getRol().getDescripcion().equals("Administrador")) {
            this.misEncuestas = ejbFacade.findAll();
        } else {
            this.misEncuestas = ejbFacade.encuestasPorUsuario(usuarioController.getLogueado().getId());
        }

        this.redireccion("/faces/encuesta/List.xhtml");
    }

    public String prepareView() {
        current = (Encuesta) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Encuesta();
        selectedItemIndex = -1;
        dimension = new DualListModel<Dimension>(new ArrayList<Dimension>(), new ArrayList<Dimension>());

        return "Create";
    }

    public String create() {
        FacesContext contexto = FacesContext.getCurrentInstance();
        ExpertoController expertoController = (ExpertoController) contexto.getApplication().evaluateExpressionGet(contexto, "#{expertoController}", ExpertoController.class);
        UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);
        EncuestaController encuestaController = (EncuestaController) contexto.getApplication().evaluateExpressionGet(contexto, "#{encuestaController}", EncuestaController.class);
        AlumnoController alumnoController = (AlumnoController) contexto.getApplication().evaluateExpressionGet(contexto, "#{alumnoController}", AlumnoController.class);
        //List<Encuestado> listaEncuestados = expertoController.getExpertosSeleccionados();

        try {
            ObjetoAprendizaje objetoAprendizaje = this.ejbObjetoAprendizaje.bucarObjetoPorIdentificador(objetoSeleccionado.getId());
            if (objetoAprendizaje == null) objetoAprendizaje = nuevoObjetoAprendizaje();
            List<Encuestado> seleccionados = new ArrayList<>();
            if (encuestaController.getCapa().equalsIgnoreCase("experto")) {
                seleccionados = expertoController.obtenerEncuestadosSeleccionados();
            } else {
                seleccionados = alumnoController.obtenerAlumnosSeleccionados();
            }

            String[] encuestados = new String[seleccionados.size()];
            /*for (Dimension d : dimension.getTarget()) {
                System.out.println("d: " + d.getId() + d.getNombre());
            }*/

            /*---------------------------------------------------DESHARCODEAR ID USUARIO CREADOR ENCUESTA ------------------------------------------------*/
            current.setObjetoaprendizaje(objetoAprendizaje);
            current.setEncuestador(usuarioController.getLogueado());
            current.setDimensiones(dimension.getTarget());
            current.setEncuestados(seleccionados);
            current.setFechacreacion(new Date());

            getFacade().edit(current);
            for (int i = 0; i < seleccionados.size(); i++) {
                encuestados[i] = seleccionados.get(i).getMail();
            }

            JsfUtil.addSuccessMessage("Encuesta creada correctamente");

            EmailController.enviarInvitacionPorEmail(encuestados, usuarioController.getLogueado().getEncuestado().getApellido() + ", " + usuarioController.getLogueado().getEncuestado().getNombre());

            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Error al crear encuesta");
            return null;
        }
    }

    private ObjetoAprendizaje nuevoObjetoAprendizaje() {
        FacesContext contexto = FacesContext.getCurrentInstance();
        ObjetoAprendizajeController objetoAprendizajeController = (ObjetoAprendizajeController) contexto.getApplication().evaluateExpressionGet(contexto, "#{objetoAprendizajeController}", ObjetoAprendizajeController.class);
        List<String> autores = new ArrayList<>();
        for (AutorDTO autor : objetoSeleccionado.getAuthor()) {
            autores.add(autor.getName());
        }
        ObjetoAprendizaje objetoAprendizaje = new ObjetoAprendizaje();
        objetoAprendizaje.setAutores(autores);
        objetoAprendizaje.setDescripcion(objetoSeleccionado.getDescription());
        objetoAprendizaje.setFechacreacion(objetoSeleccionado.getPublished());
        objetoAprendizaje.setIdentificador(objetoSeleccionado.getId());
        objetoAprendizaje.setRepositorio(objetoAprendizajeController.getRepositorioSeleccinado());
        objetoAprendizaje.setTitulo(objetoSeleccionado.getTitle());
        return objetoAprendizaje;
    }

    public String prepareEdit() {
        current = (Encuesta) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EncuestaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Encuesta) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EncuestaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Encuesta getEncuesta(int id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Encuesta.class)
    public static class EncuestaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EncuestaController controller = (EncuestaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "encuestaController");
            return controller.getEncuesta(getKey(value));
        }

        int getKey(String value) {
            int key;
            key = Integer.parseInt(value);
            return key;
        }

        String getStringKey(int value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Encuesta) {
                Encuesta o = (Encuesta) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Encuesta.class.getName());
            }
        }

    }


    boolean skip = false;


    public String onFlowProcess(FlowEvent event) {
        if (event.getNewStep().equals("fin")) {
            PrimeFaces.current().executeScript("PF('stepForward').disable()");
        } else PrimeFaces.current().executeScript("PF('stepForward').enable()");
        if (event.getNewStep().equals("personal")) {
            PrimeFaces.current().executeScript("PF('stepBackward').disable()");
        } else PrimeFaces.current().executeScript("PF('stepBackward').enable()");
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Objeto de Apredizaje seleccionado", ((ObjetoAprendizajeDTO) event.getObject()).getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Objeto de Aprendizaje deseleccionado", ((ObjetoAprendizajeDTO) event.getObject()).getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void cargarEncuestas() {
        this.enviadas = new ArrayList<>();
        this.pendientes = new ArrayList<>();
        FacesContext contexto = FacesContext.getCurrentInstance();
        EncuestadoController encuestadoController = (EncuestadoController) contexto.getApplication().evaluateExpressionGet(contexto, "#{encuestadoController}", EncuestadoController.class);
        UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);
        ResultadoEncuestaController resultadoEncuestaController = (ResultadoEncuestaController) contexto.getApplication().evaluateExpressionGet(contexto, "#{resultadoEncuestaController}", ResultadoEncuestaController.class);
        Encuestado encuestado = encuestadoController.getEjbFacade().encuestadoPorIdUsuario(usuarioController.getLogueado().getId());
        for (Encuesta encuesta : encuestado.getEncuestas()) {
            boolean bandera = false;
            List<ResultadoEncuesta> resultadoEncuestas = resultadoEncuestaController.getEjbFacade().obtenerResultadoEncuestaPorIdEncuesta(encuesta.getId());
            if (resultadoEncuestas != null) {
                for (ResultadoEncuesta re : resultadoEncuestas) {
                    if (re.getIdencuestado().getId() == encuestado.getId()) {
                        bandera = true;
                        break;
                    }
                }
            }
            if (bandera) this.enviadas.add(encuesta);
            else this.pendientes.add(encuesta);
        }
    }

    public void redireccion(String url) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + url);
        } catch (IOException ex) {
            Logger.getLogger(ValidacionPermisos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean filterByDate(Object value, Object filter, Locale locale) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        if (filter == null) {
            return true;
        }

        if (value == null) {
            return false;
        }

        return dateFormat.format(value).contains(filter.toString());

    }
}
