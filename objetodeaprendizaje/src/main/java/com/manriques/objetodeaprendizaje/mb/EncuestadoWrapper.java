package com.manriques.objetodeaprendizaje.mb;

import com.manriques.objetodeaprendizaje.modelo.Usuario;

public class EncuestadoWrapper {

    private int id;
    private String tipo;
    private String apellido;
    private String mail;
    private int edad;
    private String nombre;
    private Usuario usuario;
    private String carrera;
    private String curso;
    private String especialidad;

    public EncuestadoWrapper(int id, String tipo, String apellido, String mail, int edad, String nombre, Usuario usuario, String carrera, String curso, String especialidad) {
        this.id = id;
        this.tipo = tipo;
        this.apellido = apellido;
        this.mail = mail;
        this.edad = edad;
        this.nombre = nombre;
        this.usuario = usuario;
        this.carrera = carrera;
        this.curso = curso;
        this.especialidad = especialidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
}
