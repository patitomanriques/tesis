package com.manriques.objetodeaprendizaje.mb;

import com.manriques.objetodeaprendizaje.modelo.Encuesta;
import com.manriques.objetodeaprendizaje.modelo.ResultadoEncuesta;
import com.manriques.objetodeaprendizaje.mb.util.JsfUtil;
import com.manriques.objetodeaprendizaje.mb.util.PaginationHelper;
import com.manriques.objetodeaprendizaje.modelo.Dimension;
import com.manriques.objetodeaprendizaje.modelo.Encuestado;
import com.manriques.objetodeaprendizaje.modelo.Metrica;
import com.manriques.objetodeaprendizaje.modelo.Pregunta;
import com.manriques.objetodeaprendizaje.modelo.Respuesta;
import com.manriques.objetodeaprendizaje.modelo.RespuestaExperticiaDimension;
import com.manriques.objetodeaprendizaje.persistencia.EncuestadoFacade;
import com.manriques.objetodeaprendizaje.persistencia.ResultadoEncuestaFacade;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

import org.primefaces.component.tabview.Tab;

@Named("resultadoEncuestaController")
@SessionScoped
public class ResultadoEncuestaController implements Serializable {

    private ResultadoEncuesta current;
    private DataModel items = null;
    @EJB
    private com.manriques.objetodeaprendizaje.persistencia.ResultadoEncuestaFacade ejbFacade;

    @EJB
    private EncuestadoFacade encuestadoFacade;

    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Dimension d;
    private List<RespuestaExperticiaDimension> listaValorExperticia = new ArrayList<>();
    private List<Tab> listaTabs = new ArrayList<>();
    private Encuesta encuestaAResponder;

    public Encuesta getEncuestaAResponder() {
        return encuestaAResponder;
    }

    public void setEncuestaAResponder(Encuesta encuestaAResponder) {
        this.encuestaAResponder = encuestaAResponder;
    }

    public ResultadoEncuesta getCurrent() {
        return current;
    }

    public void setCurrent(ResultadoEncuesta current) {
        this.current = current;
    }

    public ResultadoEncuestaFacade getEjbFacade() {
        return ejbFacade;
    }

    private List<Respuesta> listaRespuesta = new ArrayList<>();

    public List<Respuesta> getListaRespuesta() {
        return listaRespuesta;
    }

    public void setListaRespuesta(List<Respuesta> listaRespuesta) {
        this.listaRespuesta = listaRespuesta;
    }


    public List<Tab> getListaTabs() {
        return listaTabs;
    }

    public void setListaTabs(List<Tab> listaTabs) {
        this.listaTabs = listaTabs;
    }

    public ResultadoEncuestaController() {
    }

    public ResultadoEncuesta getSelected() {
        if (current == null) {
            current = new ResultadoEncuesta();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ResultadoEncuestaFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        items = new ArrayDataModel<ResultadoEncuesta>((ResultadoEncuesta[]) getFacade().findAll().toArray());
        return "List";
    }

    public String prepareView() {
        current = (ResultadoEncuesta) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public void prepareCreate() {

        FacesContext contexto = FacesContext.getCurrentInstance();
        EncuestaController encuestaController = (EncuestaController) contexto.getApplication().evaluateExpressionGet(contexto, "#{encuestaController}", EncuestaController.class);
        UsuarioController usuarioController = (UsuarioController) contexto.getApplication().evaluateExpressionGet(contexto, "#{usuarioController}", UsuarioController.class);

        //Encuesta encuesta = encuestaController.getEncuesta(this.current.getEncuesta().); //HARDCODEADO LA ENCUESTA RECUPERADA
        //encuestaController.setCurrent(encuesta);


        Encuestado encuestado = encuestadoFacade.encuestadoPorIdUsuario(usuarioController.getLogueado().getId());      //HARDCODEADO ENCUESTADO RECUPERADO

        listaRespuesta = new ArrayList<>();
        listaTabs = new ArrayList<>();
        listaValorExperticia = new ArrayList<>();

        current = new ResultadoEncuesta();

        for (Dimension d : this.encuestaAResponder.getDimensiones()) {

            Tab tab1 = new Tab();
            tab1.setTitle(d.getNombre());
            listaTabs.add(tab1);

            RespuestaExperticiaDimension exper1 = new RespuestaExperticiaDimension();
            exper1.setDimension(d);
            exper1.setResultadoencuesta(current);
            listaValorExperticia.add(exper1);


            for (Metrica m : d.getMetricas()) {
                for (Pregunta p : m.getPreguntas()) {
                    Respuesta resp1 = new Respuesta();
                    resp1.setIdpregunta(p);
                    resp1.getValorrespuesta();
                    resp1.setResultado(current);
                    listaRespuesta.add(resp1);
                }
            }
        }


        current.setRespuestas(listaRespuesta);
        current.setResultadoexperticiadimension(listaValorExperticia);
        current.setIdencuestado(encuestado);


        selectedItemIndex = -1;

        redireccion("/faces/resultadoEncuesta/Create.xhtml");
        ;
//        return "Create";
    }

    public void redireccion(String url) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + url);
        } catch (IOException ex) {
            Logger.getLogger(ValidacionPermisos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Respuesta> obtenerRespuestasPorDimension(String dimension) {

        List<Respuesta> resultado = new ArrayList<>();

        for (Respuesta r : listaRespuesta) {
            if (r.getIdpregunta().getIdmetrica().getDimension().getNombre().equals(dimension)) {
                resultado.add(r);
            }
        }

        return resultado;

    }

    public RespuestaExperticiaDimension obtenerRespuestaExperticia(String dimension) {

        for (RespuestaExperticiaDimension valor1 : listaValorExperticia) {
            if (valor1.getDimension().getNombre().equals(dimension)) {
                return valor1;
            }
        }
        return null;
    }

    public boolean esExperto() {
        return this.current.getIdencuestado().getTipo().equals("Experto");
    }

    public void create() {
        FacesContext contexto = FacesContext.getCurrentInstance();
        contexto.getExternalContext().getFlash().setKeepMessages(true);
        EncuestaController encuestaController = (EncuestaController) contexto.getApplication().evaluateExpressionGet(contexto, "#{encuestaController}", EncuestaController.class);

        Encuesta encuesta = encuestaController.getEncuesta(this.encuestaAResponder.getId());

        try {
            current.setEncuesta(encuesta);
            getFacade().create(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Su respuesta fue enviada correctamente. Gracias por su tiempo."));
            redireccion("/faces/index.xhtml");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public String prepareEdit() {
        current = (ResultadoEncuesta) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ResultadoEncuestaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (ResultadoEncuesta) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ResultadoEncuestaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public ResultadoEncuesta getResultadoEncuesta(int id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = ResultadoEncuesta.class)
    public static class ResultadoEncuestaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ResultadoEncuestaController controller = (ResultadoEncuestaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "resultadoEncuestaController");
            return controller.getResultadoEncuesta(getKey(value));
        }

        int getKey(String value) {
            int key;
            key = Integer.parseInt(value);
            return key;
        }

        String getStringKey(int value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ResultadoEncuesta) {
                ResultadoEncuesta o = (ResultadoEncuesta) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + ResultadoEncuesta.class.getName());
            }
        }

    }

}
