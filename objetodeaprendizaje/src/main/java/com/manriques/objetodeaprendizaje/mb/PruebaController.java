package com.manriques.objetodeaprendizaje.mb;

import com.manriques.objetodeaprendizaje.modelo.*;
import com.manriques.objetodeaprendizaje.mb.util.JsfUtil;
import com.manriques.objetodeaprendizaje.mb.util.PaginationHelper;
import com.manriques.objetodeaprendizaje.persistencia.*;
import com.udojava.evalex.Expression;

import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("pruebaController")
@SessionScoped
public class PruebaController implements Serializable {

    @EJB
    PreguntaFacade preguntaFacade;
    @EJB
    DimensionFacade dimensionFacade;
    @EJB
    EncuestaFacade encuestaFacade;

    @EJB
    ResultadoEncuestaFacade reFacade;

    @EJB
    RespuestaExperticiaDimensionFacade respuestaExperticiaFacade;

    public PruebaController() {
    }


    public double reporteEvaluacion(Integer idDimension, Integer idCapa, Integer idMetrica, Integer idUsuario, Integer idObjetoAprendizaje) {
        // Buscar respuestaexperticiadimension con el id dimension, despues con los resultados encuesta buscar las 
        //respuestas que coincidan con las preguntas que ya se agruparon

        List<Pregunta> preguntas = this.preguntaFacade.obtenerPreguntasPorDimensionYCapa(idDimension, idCapa, idMetrica);
        List<RespuestaExperticiaDimension> experticiaDimension = this.respuestaExperticiaFacade.obtenerExperticiaPorIdDimension(idDimension, idUsuario, idObjetoAprendizaje);

        List<RespuestaExperticiaDimension> respuestasFiltradas = new ArrayList<>();
        HashMap<String, List<Integer>> sumatoria = new HashMap<>();
        int acumuladorExperticia = 0;

        for (RespuestaExperticiaDimension re : experticiaDimension) {
            for (Respuesta respuesta : re.getResultadoencuesta().getRespuestas()) {

                if (respuesta.getIdpregunta().getId() == preguntas.get(0).getId()) {
                    respuestasFiltradas.add(re);
                    break;
                }
            }
        }

        float total = 0f;
        if (idCapa == 1 && idDimension != 5) {
            total = formulaExpertoDosPreguntas(respuestasFiltradas, preguntas, 0.1f);
        }
        if (idCapa == 1 && idDimension == 5) {
            total = formulaExpertoDosPreguntas(respuestasFiltradas, preguntas, 0.2f);
        }
        if (idCapa == 2 && idDimension != 2) {
            total = formulaUsuario(respuestasFiltradas, preguntas, 1);
        }
        if (idCapa == 2 && idDimension == 2) {
            total = formulaUsuarioDosPreguntas(respuestasFiltradas, preguntas, 0.1f);
        }

        return total;
    }

    private float formulaExpertoDosPreguntas(List<RespuestaExperticiaDimension> respuestasFiltradas, List<Pregunta> preguntas, float constante) {
        HashMap<String, List<Integer>> sumatoriaRespuesta = new HashMap<>();
        int acumulador = 0;
        for (RespuestaExperticiaDimension rexd : respuestasFiltradas) {
            acumulador += rexd.getValor();
            for (Pregunta pr : preguntas) {
                for (Respuesta respuesta : rexd.getResultadoencuesta().getRespuestas()) {
                    if (pr.getCodigo().equals(respuesta.getIdpregunta().getCodigo())) {
                        if (!sumatoriaRespuesta.containsKey(respuesta.getIdpregunta().getCodigo())) {
                            List<Integer> valor = new ArrayList<>();
                            valor.add(respuesta.getValorrespuesta() * rexd.getValor());
                            sumatoriaRespuesta.put(respuesta.getIdpregunta().getCodigo(), valor);
                        } else {
                            sumatoriaRespuesta.get(respuesta.getIdpregunta().getCodigo()).add(respuesta.getValorrespuesta() * rexd.getValor());
                        }
                    }

                }
            }
        }
        float sumaValoresRespuesta = 0;
        for (Map.Entry<String, List<Integer>> respuestaValor : sumatoriaRespuesta.entrySet()) {
            int suma = 0;
            for (Integer i : respuestaValor.getValue()) {
                suma += i;
            }
            if (suma == 0 && acumulador == 0) return -1;

            float division = (float) suma / (float) acumulador;
            sumaValoresRespuesta += division;
        }

        return sumaValoresRespuesta * constante;
    }

    private float formulaUsuario(List<RespuestaExperticiaDimension> respuestasFiltradas, List<Pregunta> preguntas, float constante) {
        List<Integer> valoresRespuesta = new ArrayList<>();
        for (RespuestaExperticiaDimension rexd : respuestasFiltradas) {
            for (Pregunta pr : preguntas) {
                for (Respuesta respuesta : rexd.getResultadoencuesta().getRespuestas()) {
                    if (pr.getCodigo().equals(respuesta.getIdpregunta().getCodigo())) {
                        valoresRespuesta.add(respuesta.getValorrespuesta());
                    }
                }
            }
        }
        float sumaValoresRespuesta = 0;
        float c = (float) constante / (float) (5 * valoresRespuesta.size());
        for (Integer i : valoresRespuesta) {
            sumaValoresRespuesta += i;
        }

        return sumaValoresRespuesta != 0 ? sumaValoresRespuesta * c : 0;


    }

    private float formulaUsuarioDosPreguntas(List<RespuestaExperticiaDimension> respuestasFiltradas, List<Pregunta> preguntas, float constanteUsuario) {
        HashMap<String, List<Integer>> sumatoriaRespuesta = new HashMap<>();
        for (RespuestaExperticiaDimension rexd : respuestasFiltradas) {
            for (Pregunta pre : preguntas) {
                for (Respuesta resp : rexd.getResultadoencuesta().getRespuestas()) {
                    if (resp.getIdpregunta().getCodigo().equals(pre.getCodigo())) {
                        if (sumatoriaRespuesta.containsKey(pre.getCodigo())) {
                            sumatoriaRespuesta.get(pre.getCodigo()).add(resp.getValorrespuesta());
                        } else {
                            List<Integer> valores = new ArrayList<>();
                            valores.add(resp.getValorrespuesta());
                            sumatoriaRespuesta.put(resp.getIdpregunta().getCodigo(), valores);
                        }

                    }
                }
            }
        }
        float sumatoriaTotal = 0f;

        for (Map.Entry<String, List<Integer>> respuestaValor : sumatoriaRespuesta.entrySet()) {
            System.out.println("respuesta --->" + respuestaValor.getKey());
            int sum = 0;
            for (Integer i : respuestaValor.getValue()) {
                System.out.println("valor --->" + i);
                sum += i;

            }
            sumatoriaTotal += (float) sum / respuestaValor.getValue().size();

        }
        return constanteUsuario != 0 ? constanteUsuario * sumatoriaTotal : 0;
    }
}
