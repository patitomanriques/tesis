package com.manriques.objetodeaprendizaje.mb;

import com.manriques.objetodeaprendizaje.modelo.*;
import com.manriques.objetodeaprendizaje.mb.util.JsfUtil;
import com.manriques.objetodeaprendizaje.mb.util.PaginationHelper;
import com.manriques.objetodeaprendizaje.persistencia.RolFacade;
import com.manriques.objetodeaprendizaje.persistencia.UsuarioFacade;

import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("usuarioController")
@SessionScoped
public class UsuarioController implements Serializable {

    private Usuario current;
    private DataModel items = null;
    @EJB
    private com.manriques.objetodeaprendizaje.persistencia.UsuarioFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Usuario logueado;
    private Encuestado encuestado;
    private Alumno alumno;
    private boolean esexperto;
    private String tipo;
    private String especialidad;
    private String emailRecuperar;

    public UsuarioController() {
    }


    public UsuarioFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(UsuarioFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public String getEmailRecuperar() {
        return emailRecuperar;
    }

    public void setEmailRecuperar(String emailRecuperar) {
        this.emailRecuperar = emailRecuperar;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Alumno getAlumno() {
        if (alumno == null) {
            alumno = new Alumno();
        }
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }


    public boolean isEsexperto() {
        return esexperto;
    }

    public void setEsexperto(boolean esexperto) {
        this.esexperto = esexperto;
    }

    public Encuestado getEncuestado() {
        return encuestado;
    }

    public void setEncuestado(Encuestado encuestado) {
        this.encuestado = encuestado;
    }

    public Usuario getLogueado() {
        return logueado;
    }

    public void setearLogueado(String usuario) {
        this.logueado = this.ejbFacade.usuarioPorLogin(usuario);
        this.setLogueado(logueado);
    }

    public void setLogueado(Usuario logueado) {
        this.logueado = logueado;
    }

    public Usuario getSelected() {
        if (current == null) {
            current = new Usuario();
            selectedItemIndex = -1;
        }
        return current;
    }

    private UsuarioFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        items = new ArrayDataModel<Usuario>((Usuario[]) getFacade().findAll().toArray());
        return "List";
    }

    public String prepareView() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public void prepareCreate() {
        this.esexperto = false;
        this.especialidad = "";
        this.alumno = new Alumno();
        this.tipo = "";

        current = new Usuario();
        selectedItemIndex = -1;
        this.redireccion("/faces/usuario/Create.xhtml");
    }

    public void create() {
        try {
            FacesContext contexto = FacesContext.getCurrentInstance();
            contexto.getExternalContext().getFlash().setKeepMessages(true);
            ExpertoController expertoController = (ExpertoController) contexto.getApplication().evaluateExpressionGet(contexto, "#{expertoController}", ExpertoController.class);
            AlumnoController alumnoController = (AlumnoController) contexto.getApplication().evaluateExpressionGet(contexto, "#{alumnoController}", AlumnoController.class);
            if (ejbFacade.usuarioPorLogin(alumno.getMail()).getNombre() != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Ya existe un usuario con este mail"));
            } else {
                if (this.esexperto) {
                    Experto experto = new Experto();
                    experto.setTipo("Experto");
                    experto.setEspecialidad(this.especialidad);
                    experto.setNombre(this.alumno.getNombre());
                    experto.setApellido(this.alumno.getApellido());
                    experto.setMail(this.alumno.getMail());
                    experto.setEdad(this.alumno.getEdad());
                    this.current.setNombre(this.alumno.getMail());
                    experto.setUsuario(this.current);
                    expertoController.crearExperto(experto);
                } else {
                    Alumno alumno = new Alumno();
                    alumno.setTipo("Alumno");
                    alumno.setNombre(this.alumno.getNombre());
                    alumno.setApellido(this.alumno.getApellido());
                    alumno.setMail(this.alumno.getMail());
                    alumno.setEdad(this.alumno.getEdad());
                    alumno.setCarrera(this.alumno.getCarrera());
                    alumno.setCurso(this.alumno.getCurso());
                    this.current.setNombre(this.alumno.getMail());
                    alumno.setUsuario(this.current);
                    alumnoController.crearAlumno(alumno);
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Usuario creado correctamente"));
                this.prepareCreate();
            }
        } catch (Exception e) {
            e.printStackTrace();
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


    public String prepareEdit() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuarioUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuarioDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Usuario getUsuario(int id) {
        return ejbFacade.find(id);
    }

    Usuario consultarUsuarioPorId(int idUsuario) {
        return this.ejbFacade.find(idUsuario);
    }

    public boolean existeUsuario(String usuario, String password) {
        return this.getFacade().existeUsuario(usuario, password);
    }

    public void habilitarCampos() {
        this.esexperto = !this.tipo.equalsIgnoreCase("Alumno");

    }

    public boolean cambiaPassword(String usuario) {
        this.logueado = this.ejbFacade.usuarioPorLogin(usuario);
        return logueado.isCambiarPassword();
    }

    @FacesConverter(forClass = Usuario.class)
    public static class UsuarioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsuarioController controller = (UsuarioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usuarioController");
            return controller.getUsuario(getKey(value));
        }

        int getKey(String value) {
            int key;
            key = Integer.parseInt(value);
            return key;
        }

        String getStringKey(int value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usuario) {
                Usuario o = (Usuario) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Usuario.class.getName());
            }
        }

    }

    public boolean tienePermiso(String rol) {
        return this.getLogueado().getRol().getDescripcion().equals(rol);
    }

    public void redireccion(String url) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + url);
        } catch (IOException ex) {
            Logger.getLogger(ValidacionPermisos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void recuperar() {
        try {
            Usuario usuario = this.ejbFacade.usuarioPorLogin(this.emailRecuperar);
            if (usuario != null) {
                String contrasenia = this.generarCodigo(8);
                usuario.setCambiarPassword(true);
                usuario.setContrasenia(contrasenia);
                this.ejbFacade.edit(usuario);
                EmailController.enviarPasswordPorEmail(this.emailRecuperar, contrasenia);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "La nueva contraseña fue enviada a su correo"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Usuario no existe"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Comuniquese con un adminstrador"));
        }
    }


    public String generarCodigo(int length) {

        String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        String CHAR_UPPER = CHAR_LOWER.toUpperCase();
        String NUMBER = "0123456789";
        String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
        SecureRandom random = new SecureRandom();

        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {

            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

            // debug
            //System.out.format("%d\t:\t%c%n", rndCharAt, rndChar);

            sb.append(rndChar);

        }

        return sb.toString();
    }

    public void cambiarPassword(String password) {
        try {
            FacesContext contexto = FacesContext.getCurrentInstance();
            contexto.getExternalContext().getFlash().setKeepMessages(true);
            if (logueado != null) {
                this.logueado.setContrasenia(password);
                this.logueado.setCambiarPassword(false);
                this.ejbFacade.edit(logueado);
                logueado = null;
                redireccion("/faces/login.xhtml");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Ahora puede ingresar con su nueva contraseña"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Usuario no existe"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Comuniquese con un adminstrador"));
        }

    }

}
