/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * @author Patricia Manriques
 */
@Entity
@Table(name = "objetoaprendizaje")
@SequenceGenerator(name = "objetoaprendizaje_objetoaprendizajeid_seq", initialValue = 1, sequenceName = "objetoaprendizaje_objetoaprendizajeid_seq", allocationSize = 1)
public class ObjetoAprendizaje {

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> autores;
    @Column(columnDefinition = "TEXT")
    private String descripcion;
    private Date fechacreacion;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> formatos;

    @Id
    @Column(name = "objetoaprendizajeid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "objetoaprendizaje_objetoaprendizajeid_seq")
    private Integer id;
    private String identificador;
    private String lenguaje;
    private String tema;
    private String titulo;
    private String origen;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "repositorio_id")
    private Repositorio repositorio;

    @OneToMany(mappedBy = "objetoaprendizaje", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Encuesta> encuestas;

    public List<String> getAutores() {
        return autores;
    }

    public void setAutores(List<String> autores) {
        this.autores = autores;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public List<String> getFormatos() {
        return formatos;
    }

    public void setFormatos(List<String> formatos) {
        this.formatos = formatos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Repositorio getRepositorio() {
        return repositorio;
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public List<Encuesta> getEncuestas() {
        return encuestas;
    }

    public void setEncuestas(List<Encuesta> encuestas) {
        this.encuestas = encuestas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ObjetoAprendizaje)) {
            return false;
        }
        ObjetoAprendizaje other = (ObjetoAprendizaje) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public ObjetoAprendizaje() {
    }

    @Override
    public String toString() {
        return titulo;
    }


}
