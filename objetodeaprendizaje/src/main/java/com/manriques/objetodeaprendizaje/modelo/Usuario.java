/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "usuario")
@SequenceGenerator(name = "usuario_usuarioid_seq", initialValue = 1, sequenceName = "usuario_usuarioid_seq", allocationSize = 1)
public class Usuario {

    private String contrasenia;
    @Id
    @Column(name = "usuarioid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_usuarioid_seq")
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rol_id")
    private Rol rol;

    @Column(name = "nombre")
    private String nombre;

    @OneToOne(mappedBy = "usuario", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Encuestado encuestado;

    @Column(name = "cambiarpassword")
    private boolean cambiarPassword;

    public boolean isCambiarPassword() {
        return cambiarPassword;
    }

    public void setCambiarPassword(boolean cambiarPassword) {
        this.cambiarPassword = cambiarPassword;
    }

    public Encuestado getEncuestado() {
        return encuestado;
    }

    public void setEncuestado(Encuestado encuestado) {
        this.encuestado = encuestado;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }


    public int getId() {
        return id;
    }


    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.contrasenia);
        hash = 23 * hash + this.id;
        hash = 23 * hash + Objects.hashCode(this.rol);
        hash = 23 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.contrasenia, other.contrasenia)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

    public Usuario() {
    }

    public Usuario(String contrasenia, Rol rol, String nombre) {
        this.contrasenia = contrasenia;
        this.rol = rol;
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Usuario " + "nombre=" + nombre;
    }


}
