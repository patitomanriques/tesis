/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Patricia Manriques
 */

@Entity
@Table(name = "respuesta")
@SequenceGenerator(name="respuesta_respuestaid_seq", initialValue = 1, sequenceName = "respuesta_respuestaid_seq",allocationSize = 1)
public class Respuesta {
    
    @Id
     @Column(name = "respuestaid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respuesta_respuestaid_seq")
    private int id;
    private int valorrespuesta;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="pregunta_id")
    private Pregunta idpregunta;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="resultado_id")
    private ResultadoEncuesta resultado;

    public int getId() {
        return id;
    }

    public Pregunta getIdpregunta() {
        return idpregunta;
    }

    public void setIdpregunta(Pregunta idpregunta) {
        this.idpregunta = idpregunta;
    }

    public ResultadoEncuesta getResultado() {
        return resultado;
    }

    public void setResultado(ResultadoEncuesta resultado) {
        this.resultado = resultado;
    }


    public int getValorrespuesta() {
        return valorrespuesta;
    }

    public void setValorrespuesta(int valorrespuesta) {
        this.valorrespuesta = valorrespuesta;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.id;
        hash = 41 * hash + this.valorrespuesta;
        hash = 41 * hash + Objects.hashCode(this.idpregunta);
        hash = 41 * hash + Objects.hashCode(this.resultado);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Respuesta other = (Respuesta) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.valorrespuesta != other.valorrespuesta) {
            return false;
        }
        if (!Objects.equals(this.idpregunta, other.idpregunta)) {
            return false;
        }
        if (!Objects.equals(this.resultado, other.resultado)) {
            return false;
        }
        return true;
    }

    public Respuesta() {
    }

   

    @Override
    public String toString() {
        return "Respuesta{" + "id=" + id + '}';
    }
    
    
}
