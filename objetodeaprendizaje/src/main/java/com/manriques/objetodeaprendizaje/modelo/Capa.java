/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.criteria.Fetch;

/**
 * @author Patricia Manriques
 */
@Entity
@Table(name = "capa")
@SequenceGenerator(name = "capa_capaid_seq", initialValue = 1, sequenceName = "capa_capaid_seq", allocationSize = 1)
public class Capa {

    private String descripcion;

    @Id
    @Column(name = "capaid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "capa_capaid_seq")

    private Integer id;

    @Column(name = "nombre")
    private String nombre;

    @OneToMany(mappedBy = "idcapa", fetch = FetchType.EAGER)
    private List<Dimension> dimensiones;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public List<Dimension> getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(List<Dimension> dimensiones) {
        this.dimensiones = dimensiones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo AGREGAR DIMENSION
     *
     * @param dimension
     */
    public void agregarDimension(Dimension dimension) {

        if (dimension.getIdcapa() != this) {
            dimension.setIdcapa(this);
        }

        this.dimensiones.add(dimension);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Capa)) {
            return false;
        }
        Capa other = (Capa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombre;
    }


}
