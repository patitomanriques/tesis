package com.manriques.objetodeaprendizaje.modelo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reportedimension")
@SequenceGenerator(name = "reportedimension_reportedimensionid_seq", initialValue = 1, sequenceName = "reportedimension_reportedimensionid_seq", allocationSize = 1)
public class ReporteDimension {

    @Id
    @Column(name = "idreportedimension")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reportedimension_reportedimensionid_seq")
    private Integer idReporteDimension;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "iddimension")
    private Dimension dimension;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idreporteevaluacion")
    private ReporteEvaluacion reporteEvaluacion;

    private Double peso;

    public Integer getIdReporteDimension() {
        return idReporteDimension;
    }

    public void setIdReporteDimension(Integer idReporteDimension) {
        this.idReporteDimension = idReporteDimension;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimensiones) {
        this.dimension = dimensiones;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public ReporteEvaluacion getReporteEvaluacion() {
        return reporteEvaluacion;
    }

    public void setReporteEvaluacion(ReporteEvaluacion reporteEvaluacion) {
        this.reporteEvaluacion = reporteEvaluacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReporteDimension that = (ReporteDimension) o;
        return Objects.equals(idReporteDimension, that.idReporteDimension) &&
                Objects.equals(dimension, that.dimension) &&
                Objects.equals(reporteEvaluacion, that.reporteEvaluacion) &&
                Objects.equals(peso, that.peso);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idReporteDimension, dimension, reporteEvaluacion, peso);
    }
}
