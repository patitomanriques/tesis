/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 * @author Patricia Manriques
 */

@Entity
@Table(name = "dimension")
@SequenceGenerator(name = "dimension_dimensionid_seq", initialValue = 1, sequenceName = "dimension_dimensionid_seq", allocationSize = 1)
public class Dimension {

    private String descripcion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "capa_id")
    private Capa idcapa;

    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dimension_dimensionid_seq")
    @Id
    private int id;

    @Column(name = "nombre")
    private String nombre;

    @OneToMany(mappedBy = "dimension", fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Metrica> metricas;

    @OneToMany(mappedBy = "dimension", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ReporteDimension> reporteDimensions;

    public List<ReporteDimension> getReporteDimensions() {
        return reporteDimensions;
    }

    public void setReporteDimensions(List<ReporteDimension> reporteDimensions) {
        this.reporteDimensions = reporteDimensions;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Metrica> getMetricas() {
        return metricas;
    }

    public void setMetricas(List<Metrica> metricas) {
        this.metricas = metricas;
    }

    public Capa getIdcapa() {
        return idcapa;
    }

    public void setIdcapa(Capa idcapa) {
        this.idcapa = idcapa;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void agregarMetrica(Metrica metrica) {

        if (metrica.getDimension() != this) {
            metrica.setDimension(this);
        }

        this.metricas.add(metrica);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.descripcion);
        hash = 53 * hash + Objects.hashCode(this.idcapa);
        hash = 53 * hash + this.id;
        hash = 53 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dimension other = (Dimension) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.idcapa, other.idcapa)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return nombre;
    }


}
