/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Patricia Manriques
 */
@Entity
@Table(name = "metrica")
@SequenceGenerator(name="metrica_metricaid_seq", initialValue = 1, sequenceName = "metrica_metricaid_seq",allocationSize = 1)
public class Metrica {
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="idmetrica")
    private Formula formula;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="dimension_id")
    private Dimension dimension;
    
    @Id
     @Column(name = "metricaid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "metrica_metricaid_seq")
    private int id;
    
    private String nombre;
    
    @OneToMany(mappedBy="idmetrica", fetch = FetchType.EAGER)
    private List<Pregunta> preguntas;

    public Formula getFormula() {
        return formula;
    }

    public void setFormula(Formula formula) {
        this.formula = formula;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public int getId() {
        return id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.formula);
        hash = 53 * hash + Objects.hashCode(this.dimension);
        hash = 53 * hash + this.id;
        hash = 53 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Metrica other = (Metrica) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.formula, other.formula)) {
            return false;
        }
        if (!Objects.equals(this.dimension, other.dimension)) {
            return false;
        }
        return true;
    }

    public Metrica() {
    }

    
    @Override
    public String toString() {
        return nombre ;
    }
    
    
    
}
