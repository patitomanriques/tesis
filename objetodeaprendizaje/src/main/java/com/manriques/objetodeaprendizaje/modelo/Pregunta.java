/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Patricia Manriques
 */
@Entity
@Table(name = "pregunta")
@SequenceGenerator(name="pregunta_preguntaid_seq", initialValue = 1, sequenceName = "pregunta_preguntaid_seq",allocationSize = 1)
public class Pregunta {
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="metrica_id")
    private Metrica idmetrica;
    
    @Id
     @Column(name = "preguntaid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pregunta_preguntaid_seq")
    private int id;
    private String pregunta;
    private String codigo;

    public Metrica getIdmetrica() {
        return idmetrica;
    }

    public void setIdmetrica(Metrica idmetrica) {
        this.idmetrica = idmetrica;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }


    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.idmetrica);
        hash = 67 * hash + this.id;
        hash = 67 * hash + Objects.hashCode(this.pregunta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pregunta other = (Pregunta) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.pregunta, other.pregunta)) {
            return false;
        }
        if (!Objects.equals(this.idmetrica, other.idmetrica)) {
            return false;
        }
        return true;
    }

    public Pregunta() {
    }

    @Override
    public String toString() {
        return "Pregunta{" + id + '}';
    }
   
    
    
}
