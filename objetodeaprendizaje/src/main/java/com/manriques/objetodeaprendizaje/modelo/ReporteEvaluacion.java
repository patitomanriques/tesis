/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 * @author Patricia Manriques
 */

@Entity
@Table(name = "reporteevaluacion")
@SequenceGenerator(name = "reporteevaluacion_reporteevaluacionid_seq", initialValue = 1, sequenceName = "reporteevaluacion_reporteevaluacionid_seq", allocationSize = 1)
public class ReporteEvaluacion {

    private int cantidadencuestas;


    @Id
    @Column(name = "reporteevaluacionid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reporteevaluacion_reporteevaluacionid_seq")
    private int id;

    private float valordecalidad;

    private Date fecha;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idobjeto")
    private ObjetoAprendizaje objetoAprendizaje;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idusuario")
    private Usuario usuario;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idcapa")
    private Capa capa;

    @OneToMany(mappedBy = "reporteEvaluacion", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ReporteDimension> reporteDimensions;

    public Capa getCapa() {
        return capa;
    }

    public void setCapa(Capa capa) {
        this.capa = capa;
    }

    public List<ReporteDimension> getReporteDimensions() {
        return reporteDimensions;
    }

    public void setReporteDimensions(List<ReporteDimension> reporteDimensions) {
        this.reporteDimensions = reporteDimensions;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public ObjetoAprendizaje getObjetoAprendizaje() {
        return objetoAprendizaje;
    }

    public void setObjetoAprendizaje(ObjetoAprendizaje objetoAprendizaje) {
        this.objetoAprendizaje = objetoAprendizaje;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getCantidadencuestas() {
        return cantidadencuestas;
    }

    public void setCantidadencuestas(int cantidadencuestas) {
        this.cantidadencuestas = cantidadencuestas;
    }


    public int getId() {
        return id;
    }


    public float getValordecalidad() {
        return valordecalidad;
    }

    public void setValordecalidad(float valordecalidad) {
        this.valordecalidad = valordecalidad;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.cantidadencuestas;

        hash = 71 * hash + this.id;

        hash = 71 * hash + Float.floatToIntBits(this.valordecalidad);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReporteEvaluacion other = (ReporteEvaluacion) obj;
        if (this.cantidadencuestas != other.cantidadencuestas) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return Float.floatToIntBits(this.valordecalidad) == Float.floatToIntBits(other.valordecalidad);
    }

    public ReporteEvaluacion() {
    }


    @Override
    public String toString() {
        return "ReporteEvaluacion{" + "id=" + id + '}';
    }


}
