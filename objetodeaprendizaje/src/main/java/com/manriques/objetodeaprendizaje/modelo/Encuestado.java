/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 * @author Patricia Manriques
 */
@Entity
@Table(name = "encuestado")
@SequenceGenerator(name = "encuestado_encuestadoid_seq", initialValue = 1, sequenceName = "encuestado_encuestadoid_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo", discriminatorType = DiscriminatorType.STRING)

public abstract class Encuestado {
    @Column(name = "tipo", insertable = false, updatable = false)
    private String tipo;

    private String apellido;
    private String mail;
    private int edad;
    private String nombre;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "encuestado_encuestadoid_seq")

    private int id;

    @ManyToMany(mappedBy = "encuestados")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Encuesta> encuestas;

//        public void agregarDimension(Dimension dimension){
//        
//        if(dimension.getIdcapa() != this){
//            dimension.setIdcapa(this);
//        }
//        
//        this.dimensiones.add(dimension);
//    }


    @OneToMany(mappedBy = "idencuestado")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ResultadoEncuesta> resultados;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario", referencedColumnName = "usuarioid")
    private Usuario usuario;

    public void setId(int id) {
        this.id = id;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getApellido() {
        return apellido;
    }

    public String getTipo() {
        return tipo;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public List<ResultadoEncuesta> getResultados() {
        return resultados;
    }

    public void setResultados(List<ResultadoEncuesta> resultados) {
        this.resultados = resultados;
    }

    public List<Encuesta> getEncuestas() {
        if (encuestas == null) return new ArrayList<>();
        return encuestas;
    }

    public void setEncuestas(List<Encuesta> encuestas) {
        this.encuestas = encuestas;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.apellido);
        hash = 97 * hash + Objects.hashCode(this.mail);
        hash = 97 * hash + this.edad;
        hash = 97 * hash + Objects.hashCode(this.nombre);
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Encuestado other = (Encuestado) obj;
        if (this.edad != other.edad) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

    public Encuestado() {
    }


    @Override
    public String toString() {
        return "Encuestado{" + "apellido=" + apellido + ", edad=" + edad + ", nombre=" + nombre + '}';
    }


}
