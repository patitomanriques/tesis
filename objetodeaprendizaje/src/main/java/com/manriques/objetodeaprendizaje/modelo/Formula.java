/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Patricia Manriques
 */

@Entity
@Table(name = "formula")
@SequenceGenerator(name="formula_formulaid_seq", initialValue = 1, sequenceName = "formula_formulaid_seq",allocationSize = 1)
public class Formula {
    
    private String formula;
    
    @Id
    @Column(name = "formulaid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "formula_formulaid_seq")
    private int id;
    
    @OneToOne(mappedBy="formula")
    private Metrica idmetrica;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.formula);
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.idmetrica);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Formula other = (Formula) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.formula, other.formula)) {
            return false;
        }
        if (!Objects.equals(this.idmetrica, other.idmetrica)) {
            return false;
        }
        return true;
    }

    public Metrica getIdmetrica() {
        return idmetrica;
    }

    public void setIdmetrica(Metrica idmetrica) {
        this.idmetrica = idmetrica;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public int getId() {
        return id;
    }

    public Formula() {
    }

    

   

    @Override
    public String toString() {
        return "Formula{" + "id=" + id + '}';
    }

   
    
    
}
