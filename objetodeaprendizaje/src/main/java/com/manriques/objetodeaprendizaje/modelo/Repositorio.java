/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Patricia Manriques
 */
@Entity
@Table(name = "repositorio")
@SequenceGenerator(name="repositorio_repositorioid_seq", initialValue = 1, sequenceName = "repositorio_repositorioid_seq",allocationSize = 1)
public class Repositorio {
    
    @Id
     @Column(name = "repositorio")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "repositorio_repositorioid_seq")
    private int id;
    private String nombrerepositorio;
    
    @OneToMany(mappedBy="repositorio")
     private List<ObjetoAprendizaje> objetoaprendizaje;
    private String url;
    public List<ObjetoAprendizaje> getObjetoaprendizaje() {
        return objetoaprendizaje;
    }

    public void setObjetoaprendizaje(List<ObjetoAprendizaje> objetoaprendizaje) {
        this.objetoaprendizaje = objetoaprendizaje;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public int getId() {
        return id;
    }


    public String getNombrerepositorio() {
        return nombrerepositorio;
    }

    public void setNombrerepositorio(String nombrerepositorio) {
        this.nombrerepositorio = nombrerepositorio;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.id;
        hash = 71 * hash + Objects.hashCode(this.nombrerepositorio);
        hash = 71 * hash + Objects.hashCode(this.url);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Repositorio other = (Repositorio) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nombrerepositorio, other.nombrerepositorio)) {
            return false;
        }
        if (!Objects.equals(this.url, other.url)) {
            return false;
        }
        return true;
    }

    public Repositorio() {
    }

    

    @Override
    public String toString() {
        return this.nombrerepositorio;
    }
    
    
}
