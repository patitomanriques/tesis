/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Patricia Manriques
 */

@Entity
@Table(name = "resultadoexperticiadimension")
@SequenceGenerator(name="resultadoexperticiadimension_seq", initialValue = 1, sequenceName = "resultadoexperticiadimension_seq",allocationSize = 1)
public class RespuestaExperticiaDimension {
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="resultadoencuesta")
    ResultadoEncuesta resultadoencuesta;
    
     @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="dimension")
    Dimension dimension;
    
    
     @Id
     @Column(name = "resultadoexperticiaadimesionid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "resultadoexperticiadimension_seq")
    private int resultadoexperticiadimensionid;
     
     private int valor;

    public ResultadoEncuesta getResultadoencuesta() {
        return resultadoencuesta;
    }

    public void setResultadoencuesta(ResultadoEncuesta resultadoencuesta) {
        this.resultadoencuesta = resultadoencuesta;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public int getResultadoexperticiadimensionid() {
        return resultadoexperticiadimensionid;
    }

    public void setResultadoexperticiadimensionid(int resultadoexperticiadimensionid) {
        this.resultadoexperticiadimensionid = resultadoexperticiadimensionid;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.resultadoencuesta);
        hash = 47 * hash + Objects.hashCode(this.dimension);
        hash = 47 * hash + this.resultadoexperticiadimensionid;
        hash = 47 * hash + this.valor;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RespuestaExperticiaDimension other = (RespuestaExperticiaDimension) obj;
        if (this.resultadoexperticiadimensionid != other.resultadoexperticiadimensionid) {
            return false;
        }
        if (this.valor != other.valor) {
            return false;
        }
        if (!Objects.equals(this.resultadoencuesta, other.resultadoencuesta)) {
            return false;
        }
        if (!Objects.equals(this.dimension, other.dimension)) {
            return false;
        }
        return true;
    }
     
     
}
