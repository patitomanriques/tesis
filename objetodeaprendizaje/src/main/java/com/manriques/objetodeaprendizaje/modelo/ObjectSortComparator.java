package com.manriques.objetodeaprendizaje.modelo;

import java.util.Comparator;
import java.util.Date;

public class ObjectSortComparator implements Comparator<Encuesta> {
    @Override
    public int compare(Encuesta f1, Encuesta f2) {
        // Get the allocation priorities
        Date priority1 = f1.getFechacreacion();
        Date priority2 = f2.getFechacreacion();

        return priority1 == priority2 ? 0 : (priority1.after(priority2) ? 1 : -1);
    }
}
