/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * @author Patricia Manriques
 */
@Entity
@Table(name = "resultadoencuesta")
@SequenceGenerator(name = "resultadoencuesta_resultadoencuestaid_seq", initialValue = 1, sequenceName = "resultadoencuesta_resultadoencuestaid_seq", allocationSize = 1)
public class ResultadoEncuesta {

    @ManyToOne
    @JoinColumn(name = "encuestado_id")
    private Encuestado idencuestado;

    @Id
    @Column(name = "resultadoencuestaid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "resultadoencuesta_resultadoencuestaid_seq")
    private int id;

    @OneToMany(mappedBy = "resultado", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Respuesta> respuestas = new ArrayList<Respuesta>();

    @OneToMany(mappedBy = "resultadoencuesta", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<RespuestaExperticiaDimension> resultadoexperticiadimension = new ArrayList<RespuestaExperticiaDimension>();


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idencuesta")
    private Encuesta encuesta;

    public Encuesta getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(Encuesta encuesta) {
        this.encuesta = encuesta;
    }


    public List<RespuestaExperticiaDimension> getResultadoexperticiadimension() {
        return resultadoexperticiadimension;
    }

    public void setResultadoexperticiadimension(List<RespuestaExperticiaDimension> resultadoexperticiadimension) {
        this.resultadoexperticiadimension = resultadoexperticiadimension;
    }

    public Encuestado getIdencuestado() {
        return idencuestado;
    }


    public void setIdencuestado(Encuestado idencuestado) {
        this.idencuestado = idencuestado;
    }

    public int getId() {
        return id;
    }

    public List<Respuesta> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<Respuesta> respuestas) {
        this.respuestas = respuestas;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.idencuestado);
        hash = 83 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResultadoEncuesta other = (ResultadoEncuesta) obj;
        if (this.id != other.id) {
            return false;
        }
        return Objects.equals(this.idencuestado, other.idencuestado);
    }

    public ResultadoEncuesta() {
    }

    @Override
    public String toString() {
        return "ResultadoEncuesta{" + "id=" + id + '}';
    }


}
