/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * @author Patricia Manriques
 */

@Entity
@Table(name = "encuesta")
@SequenceGenerator(name = "encuesta_encuestaid_seq", initialValue = 1, sequenceName = "encuesta_encuestaid_seq", allocationSize = 1)
public class Encuesta {

    @ManyToMany
    @JoinTable(
            name = "ENC_DIM",
            joinColumns = @JoinColumn(name = "ENCUESTA_ID", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "DIMENSION_ID", referencedColumnName = "id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Dimension> dimensiones = new ArrayList<Dimension>();

    @ManyToMany
    @JoinTable(
            name = "ENC_ENCUESTADO",
            joinColumns = @JoinColumn(name = "ENCUESTA_ID", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ENCUESTADO_ID", referencedColumnName = "id"))
    private List<Encuestado> encuestados = new ArrayList<Encuestado>();


    /*public void agregarEncuesta(Encuesta encuesta){

       if(encuesta.getEncuestados()!= this){
           metrica.setDimension(this);
       }

       this.metricas.add(metrica);
   }*/
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "encuesta_encuestaid_seq")
    private int id;

    @Column(name = "nombre")
    private String nombre;

    private Date fechacreacion;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "objetoaprendizaje_id")
    private ObjetoAprendizaje objetoaprendizaje;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idusuario")
    private Usuario encuestador;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ResultadoEncuesta> resultadoEncuestas;

    public List<ResultadoEncuesta> getEncuestas() {
        return resultadoEncuestas;
    }

    public void setEncuestas(List<ResultadoEncuesta> resultadoEncuestas) {
        this.resultadoEncuestas = resultadoEncuestas;
    }


    public List<Dimension> getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(List<Dimension> dimensiones) {
        this.dimensiones = dimensiones;
    }

    public List<Encuestado> getEncuestados() {
        return encuestados;
    }

    public void setEncuestados(List<Encuestado> encuestados) {
        this.encuestados = encuestados;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }


    public ObjetoAprendizaje getObjetoaprendizaje() {
        return objetoaprendizaje;
    }

    public void setObjetoaprendizaje(ObjetoAprendizaje objetoaprendizaje) {
        this.objetoaprendizaje = objetoaprendizaje;
    }


    public Usuario getEncuestador() {
        return encuestador;
    }

    public void setEncuestador(Usuario encuestador) {
        this.encuestador = encuestador;
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.id;
        hash = 73 * hash + Objects.hashCode(this.nombre);

        hash = 73 * hash + Objects.hashCode(this.objetoaprendizaje);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Encuesta other = (Encuesta) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }

        return Objects.equals(this.objetoaprendizaje, other.objetoaprendizaje);
    }


    @Override
    public String toString() {
        return "Encuesta{" + "id=" + id + ", nombre=" + nombre + '}';
    }


}
