package com.manriques.objetodeaprendizaje.wrapper;

import com.manriques.objetodeaprendizaje.modelo.Dimension;

import java.util.Objects;

public class DimensionWrapper {

    private Dimension dimension;
    private double peso;

    public DimensionWrapper(Dimension dimension, double peso) {
        this.dimension = dimension;
        this.peso = peso;
    }

    public DimensionWrapper() {
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DimensionWrapper that = (DimensionWrapper) o;
        return Objects.equals(dimension, that.dimension);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dimension);
    }
}
