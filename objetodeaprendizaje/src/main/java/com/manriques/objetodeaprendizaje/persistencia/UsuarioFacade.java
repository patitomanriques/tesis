/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.Usuario;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Patricia Manriques
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    public boolean existeUsuario(String usuario, String contrasenia) {
        try {
            return em.createQuery("SELECT s FROM Usuario s WHERE s.nombre = '" + usuario + "' and s.contrasenia ='" + contrasenia + "'", Usuario.class).getResultList().size() > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Usuario usuarioPorLogin(String usuario) {
        try {
            return em.createQuery("SELECT s FROM Usuario s WHERE s.nombre = '" + usuario + "'", Usuario.class).getResultList().get(0);
        } catch (Exception e) {
            return new Usuario();
        }
    }

    public Usuario usuarioPorIdEncuestado(Integer idEncuestado) {
        try {
            return (Usuario) em.createNativeQuery("SELECT * FROM encuestado e " +
                    "INNER JOIN usuario u ON u.usuarioid = e.usuario " +
                    "WHERE e.id = :idEncuestado", Usuario.class)
                    .setParameter("idEncuestado", idEncuestado)
                    .getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
