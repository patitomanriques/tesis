/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.Encuestado;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * @author Patricia Manriques
 */
@Stateless
public class EncuestadoFacade extends AbstractFacade<Encuestado> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EncuestadoFacade() {
        super(Encuestado.class);
    }

    public Encuestado encuestadoPorIdUsuario(int id) {
        try {
            Query query = this.em.createQuery("SELECT e FROM Encuestado e WHERE e.usuario.id = :id")
                    .setParameter("id", id);
            return (Encuestado) query.getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
