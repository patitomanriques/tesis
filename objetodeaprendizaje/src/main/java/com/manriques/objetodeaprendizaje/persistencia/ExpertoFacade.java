/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.Experto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Patricia Manriques
 */
@Stateless
public class ExpertoFacade extends AbstractFacade<Experto> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ExpertoFacade() {
        super(Experto.class);
    }
    
}
