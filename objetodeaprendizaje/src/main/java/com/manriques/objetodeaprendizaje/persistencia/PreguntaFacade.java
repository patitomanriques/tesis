/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.Pregunta;
import com.manriques.objetodeaprendizaje.modelo.RespuestaExperticiaDimension;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Patricia Manriques
 */
@Stateless
public class PreguntaFacade extends AbstractFacade<Pregunta> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PreguntaFacade() {
        super(Pregunta.class);
    }
    
    public List<Pregunta> obtenerPreguntasPorDimensionYCapa(Integer idDimension, Integer idCapa, Integer idMetrica){
        
        //TypedQuery consulta = em.createQuery("SELECT re FROM ResultadoEncuesta re LEFT JOIN FETCH re.resultadoexperticiadimension LEFT JOIN FETCH re.respuestas WHERE re.id=:id", ResultadoEncuesta.class);
        TypedQuery consulta = em.createQuery("SELECT p FROM Pregunta p WHERE p.idmetrica.id = :idmetrica AND p.idmetrica.dimension.id = :iddimension "
                + "AND p.idmetrica.dimension.idcapa.id = :idcapa", Pregunta.class);
        consulta.setParameter("iddimension", idDimension);
        consulta.setParameter("idcapa", idCapa);
        consulta.setParameter("idmetrica", idMetrica);
        return (List<Pregunta>) consulta.getResultList();      
    }
    
}
