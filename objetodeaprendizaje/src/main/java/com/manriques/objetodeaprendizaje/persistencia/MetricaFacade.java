/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.Metrica;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Patricia Manriques
 */
@Stateless
public class MetricaFacade extends AbstractFacade<Metrica> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MetricaFacade() {
        super(Metrica.class);
    }

    public List<Metrica> consultarMetricaPorCapa(int id) {
        TypedQuery query = em.createQuery("SELECT me FROM Metrica me WHERE me.dimension.idcapa.id = :idCapa", Metrica.class)
                .setParameter("idCapa", id);
        return (List<Metrica>) query.getResultList();
    }
}
