/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.ResultadoEncuesta;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Patricia Manriques
 */
@Stateless
public class ResultadoEncuestaFacade extends AbstractFacade<ResultadoEncuesta> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ResultadoEncuestaFacade() {
        super(ResultadoEncuesta.class);
    }

    public ResultadoEncuesta obtenerResultadoEncuestaPorId(Integer id) {

        //TypedQuery consulta = em.createQuery("SELECT re FROM ResultadoEncuesta re LEFT JOIN FETCH re.resultadoexperticiadimension LEFT JOIN FETCH re.respuestas WHERE re.id=:id", ResultadoEncuesta.class);
        TypedQuery consulta = em.createQuery("SELECT re FROM ResultadoEncuesta re WHERE re.id=:id", ResultadoEncuesta.class);
        consulta.setParameter("id", id);
        return (ResultadoEncuesta) consulta.getSingleResult();
    }

    public List<ResultadoEncuesta> obtenerResultadoEncuestaPorIdEncuesta(int id) {
        try {
            Query query = em.createQuery("SELECT re FROM ResultadoEncuesta re WHERE re.encuesta.id = :id")
                    .setParameter("id", id);
            if (query.getResultList() != null) return query.getResultList();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
