/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.ObjetoAprendizaje;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * @author Patricia Manriques
 */
@Stateless
public class ObjetoAprendizajeFacade extends AbstractFacade<ObjetoAprendizaje> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ObjetoAprendizajeFacade() {
        super(ObjetoAprendizaje.class);
    }

    public ObjetoAprendizaje bucarObjetoPorIdentificador(String identificador) {
        Query consulta = em.createNativeQuery("SELECT * FROM objetoaprendizaje WHERE identificador = :identificador", ObjetoAprendizaje.class)
                .setParameter("identificador", identificador);
        if (consulta.getResultList().size() != 0) {
            return (ObjetoAprendizaje) consulta.getResultList().get(0);
        }
        return null;

    }
}
