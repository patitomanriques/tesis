/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.Capa;
import com.manriques.objetodeaprendizaje.modelo.Dimension;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Patricia Manriques
 */
@Stateless
public class DimensionFacade extends AbstractFacade<Dimension> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Dimension obtenerDimensionPorId(int id){
        
        TypedQuery consulta = em.createQuery("SELECT DISTINCT d FROM Dimension d LEFT JOIN FETCH d.metricas WHERE d.id = :id", Dimension.class);
        consulta.setParameter("id", id);
        
        Dimension d = (Dimension)consulta.getSingleResult();
        System.out.println("Metricas de Dimension " + d.getNombre() + " : " + d.getMetricas().size());
        
        return d;

    }

    public DimensionFacade() {
        super(Dimension.class);
    }
    
    public List<Dimension> buscardimensionesporcapa( int idcapa){
        return em.createQuery("select d from Dimension d where d.idcapa.id= :capaid").setParameter("capaid", idcapa).getResultList();
        
    }
    

    
}
