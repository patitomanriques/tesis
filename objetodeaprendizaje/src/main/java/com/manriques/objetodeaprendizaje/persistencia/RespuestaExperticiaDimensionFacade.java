/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.RespuestaExperticiaDimension;
import com.manriques.objetodeaprendizaje.modelo.ResultadoEncuesta;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * @author Patricia Manriques
 */
@Stateless
public class RespuestaExperticiaDimensionFacade extends AbstractFacade<RespuestaExperticiaDimension> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RespuestaExperticiaDimensionFacade() {
        super(RespuestaExperticiaDimension.class);
    }

    public List<RespuestaExperticiaDimension> obtenerExperticiaporId(Integer id) {

        //TypedQuery consulta = em.createQuery("SELECT re FROM ResultadoEncuesta re LEFT JOIN FETCH re.resultadoexperticiadimension LEFT JOIN FETCH re.respuestas WHERE re.id=:id", ResultadoEncuesta.class);
        TypedQuery consulta = em.createQuery("SELECT re FROM RespuestaExperticiaDimension re WHERE re.resultadoexperticiadimensionid:id", RespuestaExperticiaDimension.class);
        consulta.setParameter("id", id);
        return (List<RespuestaExperticiaDimension>) consulta.getSingleResult();
    }

    public RespuestaExperticiaDimension obtenerExperticiaporIdDimension(Integer id) {

        //TypedQuery consulta = em.createQuery("SELECT re FROM ResultadoEncuesta re LEFT JOIN FETCH re.resultadoexperticiadimension LEFT JOIN FETCH re.respuestas WHERE re.id=:id", ResultadoEncuesta.class);
        TypedQuery consulta = em.createQuery("SELECT re FROM RespuestaExperticiaDimension re WHERE re.dimension.id = :id", RespuestaExperticiaDimension.class);
        consulta.setParameter("id", id);
        return (RespuestaExperticiaDimension) consulta.getSingleResult();
    }

    public List<RespuestaExperticiaDimension> obtenerExperticiaPorIdDimension(Integer idDimension, Integer idUsuario, Integer idObjetoAprendizaje) {
        Query consulta = em.createNativeQuery("select rex.* " +
                "from resultadoexperticiadimension rex " +
                "inner join resultadoencuesta re on re.resultadoencuestaid = rex.resultadoencuesta " +
                "inner join encuesta e on e.id = re.idencuesta " +
                "where dimension = :idDimension and e.idusuario = :idUsuario and e.objetoaprendizaje_id = :idObjetoAprendizaje", RespuestaExperticiaDimension.class);
        consulta.setParameter("idDimension", idDimension).setParameter("idUsuario", idUsuario).setParameter("idObjetoAprendizaje", idObjetoAprendizaje);
        return (List<RespuestaExperticiaDimension>) consulta.getResultList();
    }
}
