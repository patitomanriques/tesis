/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manriques.objetodeaprendizaje.persistencia;

import com.manriques.objetodeaprendizaje.modelo.Encuesta;
import com.manriques.objetodeaprendizaje.modelo.ReporteEvaluacion;
import com.manriques.objetodeaprendizaje.modelo.RespuestaExperticiaDimension;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Patricia Manriques
 */
@Stateless
public class ReporteEvaluacionFacade extends AbstractFacade<ReporteEvaluacion> {

    @PersistenceContext(unitName = "com.manriques_objetodeaprendizaje_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReporteEvaluacionFacade() {
        super(ReporteEvaluacion.class);
    }

    public List<Encuesta> consultarEncuestasPorUsuarioYObjeto(Integer idUsuario, Integer idObjeto) {
        TypedQuery consulta = em.createQuery("SELECT en FROM Encuesta en WHERE en.encuestador.id = :idUsuario AND en.objetoaprendizaje.id = :idObjeto", Encuesta.class)
                .setParameter("idUsuario", idUsuario).setParameter("idObjeto", idObjeto);
        return (List<Encuesta>) consulta.getResultList();
    }

    public List<ReporteEvaluacion> reportesPorUsuario(int id) {
        TypedQuery consulta = em.createQuery("SELECT re FROM ReporteEvaluacion re WHERE re.usuario.id = :id", ReporteEvaluacion.class)
                .setParameter("id", id);
        return (List<ReporteEvaluacion>) consulta.getResultList();
    }
}
