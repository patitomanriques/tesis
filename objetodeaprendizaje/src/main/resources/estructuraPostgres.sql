
    alter table ENC_DIM 
        drop constraint FK_a3d707460abd4b6995e4bfae164;
    alter table ENC_DIM 
        drop constraint FK_11c77292f343401780ea9a87675;
    alter table ENC_ENCUESTADO 
        drop constraint FK_483f69571e5440eda2b09fa77e2;
    alter table ENC_ENCUESTADO 
        drop constraint FK_1afaa278c9cb4d2eb99e1a70302;
    alter table ObjetoAprendizaje_autores 
        drop constraint FK_9f705cad74484f35b0db67b21eb;
    alter table ObjetoAprendizaje_formatos 
        drop constraint FK_7b637e451a8546b898d8279c031;
    alter table dimension 
        drop constraint FK_31f91b79b8c04c75acd5819180b;
    alter table encuesta 
        drop constraint FK_70849ea68d8d422a88b5e1cc7bf;
    alter table encuesta 
        drop constraint FK_537bb541ba45494dbca7a622c12;
    alter table encuesta_resultadoencuesta 
        drop constraint FK_458f8c3640684017b09e8104488;
    alter table encuesta_resultadoencuesta 
        drop constraint FK_4f138961c5d44eb5847f2756953;
    alter table encuestado 
        drop constraint FK_1a7630563eb04a46ae46e41eeb4;
    alter table metrica 
        drop constraint FK_3b193997e301480e89df69ce3c4;
    alter table metrica 
        drop constraint FK_0348d1983aa54202b9fe4d565ae;
    alter table objetoaprendizaje 
        drop constraint FK_ecc0c4d9a6c1410295bd8a7fbef;
    alter table pregunta 
        drop constraint FK_95b5ea2b33fd436aaaabb90afbc;
    alter table reportedimension 
        drop constraint FK_a2c7b13bd7304f89ae60cd99b2b;
    alter table reportedimension 
        drop constraint FK_3c007f3a527f45d39c759a7ff4a;
    alter table reporteevaluacion 
        drop constraint FK_3254efb16a8b452db5374d064c8;
    alter table reporteevaluacion 
        drop constraint FK_873147b87eac4031b9542d040e7;
    alter table reporteevaluacion 
        drop constraint FK_1cfb9a611cf34c92be49cf973be;
    alter table respuesta 
        drop constraint FK_00fb30337851408394a7d132e6b;
    alter table respuesta 
        drop constraint FK_45e670100e5649db9ef26ff47ed;
    alter table resultadoencuesta 
        drop constraint FK_61d086b0f7a24f64b7132bfc99e;
    alter table resultadoencuesta 
        drop constraint FK_2b34f83be41d4342b36337e587b;
    alter table resultadoexperticiadimension 
        drop constraint FK_9913bcc562b54fd58b06eafc6b9;
    alter table resultadoexperticiadimension 
        drop constraint FK_15dac3daf65241d9a9f294e4b42;
    alter table usuario 
        drop constraint FK_c63624c0760b4d56beba0eb6af1;
    drop table if exists ENC_DIM cascade;
    drop table if exists ENC_ENCUESTADO cascade;
    drop table if exists ObjetoAprendizaje_autores cascade;
    drop table if exists ObjetoAprendizaje_formatos cascade;
    drop table if exists capa cascade;
    drop table if exists dimension cascade;
    drop table if exists encuesta cascade;
    drop table if exists encuesta_resultadoencuesta cascade;
    drop table if exists encuestado cascade;
    drop table if exists formula cascade;
    drop table if exists metrica cascade;
    drop table if exists objetoaprendizaje cascade;
    drop table if exists pregunta cascade;
    drop table if exists reportedimension cascade;
    drop table if exists reporteevaluacion cascade;
    drop table if exists repositorio cascade;
    drop table if exists respuesta cascade;
    drop table if exists resultadoencuesta cascade;
    drop table if exists resultadoexperticiadimension cascade;
    drop table if exists rol cascade;
    drop table if exists usuario cascade;
    drop sequence capa_capaid_seq;
    drop sequence dimension_dimensionid_seq;
    drop sequence encuesta_encuestaid_seq;
    drop sequence encuestado_encuestadoid_seq;
    drop sequence formula_formulaid_seq;
    drop sequence metrica_metricaid_seq;
    drop sequence objetoaprendizaje_objetoaprendizajeid_seq;
    drop sequence pregunta_preguntaid_seq;
    drop sequence reportedimension_reportedimensionid_seq;
    drop sequence reporteevaluacion_reporteevaluacionid_seq;
    drop sequence repositorio_repositorioid_seq;
    drop sequence respuesta_respuestaid_seq;
    drop sequence resultadoencuesta_resultadoencuestaid_seq;
    drop sequence resultadoexperticiadimension_seq;
    drop sequence rol_rolid_seq;
    drop sequence usuario_usuarioid_seq;
    create table ENC_DIM (
        ENCUESTA_ID int4 not null,
        DIMENSION_ID int4 not null
    );
    create table ENC_ENCUESTADO (
        ENCUESTA_ID int4 not null,
        ENCUESTADO_ID int4 not null
    );
    create table ObjetoAprendizaje_autores (
        ObjetoAprendizaje_objetoaprendizajeid int4 not null,
        autores varchar(255)
    );
    create table ObjetoAprendizaje_formatos (
        ObjetoAprendizaje_objetoaprendizajeid int4 not null,
        formatos varchar(255)
    );
    create table capa (
        capaid int4 not null,
        descripcion varchar(255),
        nombre varchar(255),
        primary key (capaid)
    );
    create table dimension (
        id int4 not null,
        descripcion varchar(255),
        nombre varchar(255),
        capa_id int4,
        primary key (id)
    );
    create table encuesta (
        id int4 not null,
        fechacreacion timestamp,
        nombre varchar(255),
        idusuario int4,
        objetoaprendizaje_id int4,
        primary key (id)
    );
    create table encuesta_resultadoencuesta (
        encuesta_id int4 not null,
        resultadoEncuestas_resultadoencuestaid int4 not null
    );
    create table encuestado (
        tipo varchar(31) not null,
        id int4 not null,
        apellido varchar(255),
        edad int4 not null,
        mail varchar(255),
        nombre varchar(255),
        carrera varchar(255),
        curso varchar(255),
        especialidad varchar(255),
        usuario int4,
        primary key (id)
    );
    create table formula (
        formulaid int4 not null,
        formula varchar(255),
        primary key (formulaid)
    );
    create table metrica (
        metricaid int4 not null,
        nombre varchar(255),
        dimension_id int4,
        idmetrica int4,
        primary key (metricaid)
    );
    create table objetoaprendizaje (
        objetoaprendizajeid int4 not null,
        descripcion TEXT,
        fechacreacion timestamp,
        identificador varchar(255),
        lenguaje varchar(255),
        origen varchar(255),
        tema varchar(255),
        titulo varchar(255),
        repositorio_id int4,
        primary key (objetoaprendizajeid)
    );
    create table pregunta (
        preguntaid int4 not null,
        codigo varchar(255),
        pregunta varchar(255),
        metrica_id int4,
        primary key (preguntaid)
    );
    create table reportedimension (
        idreportedimension int4 not null,
        peso float8,
        iddimension int4,
        idreporteevaluacion int4,
        primary key (idreportedimension)
    );
    create table reporteevaluacion (
        reporteevaluacionid int4 not null,
        cantidadencuestas int4 not null,
        fecha timestamp,
        valordecalidad float4 not null,
        idcapa int4,
        idobjeto int4,
        idusuario int4,
        primary key (reporteevaluacionid)
    );
    create table repositorio (
        repositorio int4 not null,
        nombrerepositorio varchar(255),
        url varchar(255),
        primary key (repositorio)
    );
    create table respuesta (
        respuestaid int4 not null,
        valorrespuesta int4 not null,
        pregunta_id int4,
        resultado_id int4,
        primary key (respuestaid)
    );
    create table resultadoencuesta (
        resultadoencuestaid int4 not null,
        idencuesta int4,
        encuestado_id int4,
        primary key (resultadoencuestaid)
    );
    create table resultadoexperticiadimension (
        resultadoexperticiaadimesionid int4 not null,
        valor int4 not null,
        dimension int4,
        resultadoencuesta int4,
        primary key (resultadoexperticiaadimesionid)
    );
    create table rol (
        rolid int4 not null,
        descripcion varchar(255),
        primary key (rolid)
    );
    create table usuario (
        usuarioid int4 not null,
        contrasenia varchar(255),
        nombre varchar(255),
        rol_id int4,
        primary key (usuarioid)
    );
    alter table ENC_DIM 
        add constraint FK_a3d707460abd4b6995e4bfae164 
        foreign key (DIMENSION_ID) 
        references dimension;
    alter table ENC_DIM 
        add constraint FK_11c77292f343401780ea9a87675 
        foreign key (ENCUESTA_ID) 
        references encuesta;
    alter table ENC_ENCUESTADO 
        add constraint FK_483f69571e5440eda2b09fa77e2 
        foreign key (ENCUESTADO_ID) 
        references encuestado;
    alter table ENC_ENCUESTADO 
        add constraint FK_1afaa278c9cb4d2eb99e1a70302 
        foreign key (ENCUESTA_ID) 
        references encuesta;
    alter table ObjetoAprendizaje_autores 
        add constraint FK_9f705cad74484f35b0db67b21eb 
        foreign key (ObjetoAprendizaje_objetoaprendizajeid) 
        references objetoaprendizaje;
    alter table ObjetoAprendizaje_formatos 
        add constraint FK_7b637e451a8546b898d8279c031 
        foreign key (ObjetoAprendizaje_objetoaprendizajeid) 
        references objetoaprendizaje;
    alter table dimension 
        add constraint FK_31f91b79b8c04c75acd5819180b 
        foreign key (capa_id) 
        references capa;
    alter table encuesta 
        add constraint FK_70849ea68d8d422a88b5e1cc7bf 
        foreign key (idusuario) 
        references usuario;
    alter table encuesta 
        add constraint FK_537bb541ba45494dbca7a622c12 
        foreign key (objetoaprendizaje_id) 
        references objetoaprendizaje;
    alter table encuesta_resultadoencuesta 
        add constraint UK_8043fa96444e4716be6117ce4f5 unique (resultadoEncuestas_resultadoencuestaid);
    alter table encuesta_resultadoencuesta 
        add constraint FK_458f8c3640684017b09e8104488 
        foreign key (resultadoEncuestas_resultadoencuestaid) 
        references resultadoencuesta;
    alter table encuesta_resultadoencuesta 
        add constraint FK_4f138961c5d44eb5847f2756953 
        foreign key (encuesta_id) 
        references encuesta;
    alter table encuestado 
        add constraint FK_1a7630563eb04a46ae46e41eeb4 
        foreign key (usuario) 
        references usuario;
    alter table metrica 
        add constraint FK_3b193997e301480e89df69ce3c4 
        foreign key (dimension_id) 
        references dimension;
    alter table metrica 
        add constraint FK_0348d1983aa54202b9fe4d565ae 
        foreign key (idmetrica) 
        references formula;
    alter table objetoaprendizaje 
        add constraint FK_ecc0c4d9a6c1410295bd8a7fbef 
        foreign key (repositorio_id) 
        references repositorio;
    alter table pregunta 
        add constraint FK_95b5ea2b33fd436aaaabb90afbc 
        foreign key (metrica_id) 
        references metrica;
    alter table reportedimension 
        add constraint FK_a2c7b13bd7304f89ae60cd99b2b 
        foreign key (iddimension) 
        references dimension;
    alter table reportedimension 
        add constraint FK_3c007f3a527f45d39c759a7ff4a 
        foreign key (idreporteevaluacion) 
        references reporteevaluacion;
    alter table reporteevaluacion 
        add constraint FK_3254efb16a8b452db5374d064c8 
        foreign key (idcapa) 
        references capa;
    alter table reporteevaluacion 
        add constraint FK_873147b87eac4031b9542d040e7 
        foreign key (idobjeto) 
        references objetoaprendizaje;
    alter table reporteevaluacion 
        add constraint FK_1cfb9a611cf34c92be49cf973be 
        foreign key (idusuario) 
        references usuario;
    alter table respuesta 
        add constraint FK_00fb30337851408394a7d132e6b 
        foreign key (pregunta_id) 
        references pregunta;
    alter table respuesta 
        add constraint FK_45e670100e5649db9ef26ff47ed 
        foreign key (resultado_id) 
        references resultadoencuesta;
    alter table resultadoencuesta 
        add constraint FK_61d086b0f7a24f64b7132bfc99e 
        foreign key (idencuesta) 
        references encuesta;
    alter table resultadoencuesta 
        add constraint FK_2b34f83be41d4342b36337e587b 
        foreign key (encuestado_id) 
        references encuestado;
    alter table resultadoexperticiadimension 
        add constraint FK_9913bcc562b54fd58b06eafc6b9 
        foreign key (dimension) 
        references dimension;
    alter table resultadoexperticiadimension 
        add constraint FK_15dac3daf65241d9a9f294e4b42 
        foreign key (resultadoencuesta) 
        references resultadoencuesta;
    alter table usuario 
        add constraint FK_c63624c0760b4d56beba0eb6af1 
        foreign key (rol_id) 
        references rol;
    create sequence capa_capaid_seq;
    create sequence dimension_dimensionid_seq;
    create sequence encuesta_encuestaid_seq;
    create sequence encuestado_encuestadoid_seq;
    create sequence formula_formulaid_seq;
    create sequence metrica_metricaid_seq;
    create sequence objetoaprendizaje_objetoaprendizajeid_seq;
    create sequence pregunta_preguntaid_seq;
    create sequence reportedimension_reportedimensionid_seq;
    create sequence reporteevaluacion_reporteevaluacionid_seq;
    create sequence repositorio_repositorioid_seq;
    create sequence respuesta_respuestaid_seq;
    create sequence resultadoencuesta_resultadoencuestaid_seq;
    create sequence resultadoexperticiadimension_seq;
    create sequence rol_rolid_seq;
    create sequence usuario_usuarioid_seq;