--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.3
-- Started on 2020-01-14 19:19:15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 207 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2149 (class 0 OID 0)
-- Dependencies: 207
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 32799)
-- Name: capa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE capa (
    capaid integer NOT NULL,
    descripcion character varying(255),
    nombre character varying(255)
);


ALTER TABLE public.capa OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 32998)
-- Name: capa_capaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE capa_capaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.capa_capaid_seq OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 32807)
-- Name: dimension; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dimension (
    id integer NOT NULL,
    descripcion character varying(255),
    nombre character varying(255),
    capa_id integer
);


ALTER TABLE public.dimension OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 33000)
-- Name: dimension_dimensionid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dimension_dimensionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dimension_dimensionid_seq OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 32778)
-- Name: enc_dim; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE enc_dim (
    encuesta_id integer NOT NULL,
    dimension_id integer NOT NULL
);


ALTER TABLE public.enc_dim OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 32781)
-- Name: enc_encuestado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE enc_encuestado (
    encuesta_id integer NOT NULL,
    encuestado_id integer NOT NULL
);


ALTER TABLE public.enc_encuestado OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 32815)
-- Name: encuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE encuesta (
    id integer NOT NULL,
    nombre character varying(255),
    nombremetrica character varying(255),
    objetoaprendizaje_id integer,
    encuesta integer,
    fechacreacion timestamp with time zone
);


ALTER TABLE public.encuesta OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 33002)
-- Name: encuesta_encuestaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE encuesta_encuestaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.encuesta_encuestaid_seq OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 32823)
-- Name: encuestado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE encuestado (
    tipo character varying(31) NOT NULL,
    id integer NOT NULL,
    apellido character varying(255),
    edad integer NOT NULL,
    mail character varying(255),
    nombre character varying(255),
    carrera character varying(255),
    curso character varying(255),
    especialidad character varying(255),
    usuario integer
);


ALTER TABLE public.encuestado OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 33004)
-- Name: encuestado_encuestadoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE encuestado_encuestadoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.encuestado_encuestadoid_seq OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 32831)
-- Name: formula; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE formula (
    formulaid integer NOT NULL,
    formula character varying(255)
);


ALTER TABLE public.formula OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 33006)
-- Name: formula_formulaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE formula_formulaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formula_formulaid_seq OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 32836)
-- Name: metrica; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE metrica (
    metricaid integer NOT NULL,
    nombre character varying(255),
    dimension_id integer,
    idmetrica integer
);


ALTER TABLE public.metrica OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 33008)
-- Name: metrica_metricaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE metrica_metricaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metrica_metricaid_seq OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 32841)
-- Name: objetoapredindizaje; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE objetoapredindizaje (
    objetoaprendizajeid integer NOT NULL,
    creador character varying(255),
    fechacreacion bytea,
    identificador character varying(255),
    lenguaje character varying(255),
    nombre character varying(255),
    titulo character varying(255),
    ubicacion character varying(255),
    repositorio_id integer
);


ALTER TABLE public.objetoapredindizaje OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 32784)
-- Name: objetoaprendizaje_contribuyente; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE objetoaprendizaje_contribuyente (
    objetoaprendizaje_objetoaprendizajeid integer NOT NULL,
    contribuyente character varying(255)
);


ALTER TABLE public.objetoaprendizaje_contribuyente OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 32787)
-- Name: objetoaprendizaje_descripcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE objetoaprendizaje_descripcion (
    objetoaprendizaje_objetoaprendizajeid integer NOT NULL,
    descripcion character varying(255)
);


ALTER TABLE public.objetoaprendizaje_descripcion OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 32790)
-- Name: objetoaprendizaje_formatos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE objetoaprendizaje_formatos (
    objetoaprendizaje_objetoaprendizajeid integer NOT NULL,
    formatos character varying(255)
);


ALTER TABLE public.objetoaprendizaje_formatos OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 33010)
-- Name: objetoaprendizaje_objetoaprendizajeid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE objetoaprendizaje_objetoaprendizajeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.objetoaprendizaje_objetoaprendizajeid_seq OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 32793)
-- Name: objetoaprendizaje_tema; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE objetoaprendizaje_tema (
    objetoaprendizaje_objetoaprendizajeid integer NOT NULL,
    tema character varying(255)
);


ALTER TABLE public.objetoaprendizaje_tema OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 32796)
-- Name: objetoaprendizaje_tipos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE objetoaprendizaje_tipos (
    objetoaprendizaje_objetoaprendizajeid integer NOT NULL,
    tipos character varying(255)
);


ALTER TABLE public.objetoaprendizaje_tipos OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 32849)
-- Name: pregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pregunta (
    preguntaid integer NOT NULL,
    pregunta character varying(255),
    metrica_id integer,
    codigo character varying(10)
);


ALTER TABLE public.pregunta OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 33012)
-- Name: pregunta_preguntaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pregunta_preguntaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pregunta_preguntaid_seq OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 32854)
-- Name: reporteevaluacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE reporteevaluacion (
    reporteevaluacionid integer NOT NULL,
    cantidadencuestas integer NOT NULL,
    valordecalidad real NOT NULL
);


ALTER TABLE public.reporteevaluacion OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 33014)
-- Name: reporteevaluacion_reporteevaluacionid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reporteevaluacion_reporteevaluacionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reporteevaluacion_reporteevaluacionid_seq OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 32859)
-- Name: repositorio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE repositorio (
    repositorio integer NOT NULL,
    nombrerepositorio character varying(255),
    url character varying(255)
);


ALTER TABLE public.repositorio OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 33016)
-- Name: repositorio_repositorioid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE repositorio_repositorioid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.repositorio_repositorioid_seq OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 32867)
-- Name: respuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE respuesta (
    respuestaid integer NOT NULL,
    valorrespuesta integer NOT NULL,
    pregunta_id integer,
    resultado_id integer
);


ALTER TABLE public.respuesta OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 33018)
-- Name: respuesta_respuestaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE respuesta_respuestaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.respuesta_respuestaid_seq OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 32872)
-- Name: resultadoencuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE resultadoencuesta (
    resultadoencuestaid integer NOT NULL,
    encuestado_id integer,
    reporte_id integer
);


ALTER TABLE public.resultadoencuesta OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 33020)
-- Name: resultadoencuesta_resultadoencuestaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE resultadoencuesta_resultadoencuestaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.resultadoencuesta_resultadoencuestaid_seq OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 40969)
-- Name: resultadoexperticiadimension; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE resultadoexperticiadimension (
    resultadoexperticiaadimesionid integer NOT NULL,
    valor integer NOT NULL,
    dimension integer,
    resultadoencuesta integer
);


ALTER TABLE public.resultadoexperticiadimension OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 40984)
-- Name: resultadoexperticiadimension_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE resultadoexperticiadimension_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.resultadoexperticiadimension_seq OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 32880)
-- Name: rol; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rol (
    rolid integer NOT NULL,
    descripcion character varying(255)
);


ALTER TABLE public.rol OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 33022)
-- Name: rol_rolid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rol_rolid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rol_rolid_seq OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 32885)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    usuarioid integer NOT NULL,
    contrasenia character varying(255),
    nombre character varying(255),
    rol_id integer,
    encuestado integer
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 33024)
-- Name: usuario_usuarioid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_usuarioid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_usuarioid_seq OWNER TO postgres;

--
-- TOC entry 2112 (class 0 OID 32799)
-- Dependencies: 177
-- Data for Name: capa; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO capa VALUES (1, 'Capa para profesores o expertos sobre el tema que se trata', 'Experto');
INSERT INTO capa VALUES (2, 'Capa para Alumnos', 'Gestion de Usuarios');


--
-- TOC entry 2150 (class 0 OID 0)
-- Dependencies: 191
-- Name: capa_capaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('capa_capaid_seq', 2, true);


--
-- TOC entry 2113 (class 0 OID 32807)
-- Dependencies: 178
-- Data for Name: dimension; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO dimension VALUES (1, 'Esta enfocado en aspectos relacionados al proceso de enseñanza-aprendizaje', 'Educativa', 1);
INSERT INTO dimension VALUES (2, 'Analiza aspectos temáticos y disciplinares con el fin de identificar la claridad y rigurosidad de los contenidos', 'Contenido', 1);
INSERT INTO dimension VALUES (3, 'Evalúa aspectos relacionados con la parte visual del contenido de los OAs', 'Estética', 1);
INSERT INTO dimension VALUES (4, 'Evalua aspectos como la disponibilidad y acceso a los contenidos, la forma como los usuarios pueden interactuar con los recursos', 'Funcional ', 1);
INSERT INTO dimension VALUES (5, 'Se analiza que tan completos y bien estructurados se encuentran los metadatos de cada OA', 'Metadatos', 1);
INSERT INTO dimension VALUES (6, 'Evalua aspectos relacionados con el proceso de enseñanza-aprendizaje', 'Educativa', 2);
INSERT INTO dimension VALUES (7, 'Evalua aspectos relacionados con la parte visual del contenido de los OAs', 'Estetica', 2);
INSERT INTO dimension VALUES (8, 'Evalua aspectos como la disponibilidad y acceso a los contenidos, la forma como los usuarios pueden interactuar con los recursos', 'Funcional', 2);
INSERT INTO dimension VALUES (9, 'Analiza que tan completos y bien estructurados se encuentran los metadatos de cada OA', 'Metadatos', 2);
INSERT INTO dimension VALUES (10, 'Analiza los OAs respecto a los demas objetos en el repositorio de acuerdo a diferentes metricas, identificando cuales son los que tienen mejores caracteristicas frente a este contexto', 'Contextual', 2);


--
-- TOC entry 2151 (class 0 OID 0)
-- Dependencies: 192
-- Name: dimension_dimensionid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dimension_dimensionid_seq', 10, true);


--
-- TOC entry 2105 (class 0 OID 32778)
-- Dependencies: 170
-- Data for Name: enc_dim; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2106 (class 0 OID 32781)
-- Dependencies: 171
-- Data for Name: enc_encuestado; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2114 (class 0 OID 32815)
-- Dependencies: 179
-- Data for Name: encuesta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2152 (class 0 OID 0)
-- Dependencies: 193
-- Name: encuesta_encuestaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('encuesta_encuestaid_seq', 1, false);


--
-- TOC entry 2115 (class 0 OID 32823)
-- Dependencies: 180
-- Data for Name: encuestado; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO encuestado VALUES ('Alumno', 1, 'perez', 25, 'juanperez@hotmail.com', 'juan', 'Ingenieria en sistemas', '5to', NULL, NULL);
INSERT INTO encuestado VALUES ('Alumno', 3, 'Pozo', 24, 'mpozo@gmail.com', 'Mauricio', 'Licenciatura en Sistemas', '4to', NULL, NULL);
INSERT INTO encuestado VALUES ('Alumno', 4, 'Zalazar', 21, 'juanzalazar@hormail.com', 'Juan', 'Licenciatura en Sistema', '1ero', NULL, NULL);
INSERT INTO encuestado VALUES ('Alumno', 5, 'Paez', 25, 'paezdaniel@hotmail.com', 'Daniel ', 'Ingenieria en Sistemas', '4to', NULL, NULL);
INSERT INTO encuestado VALUES ('Experto', 6, 'Chade', 40, 'chadepablo@hotmail.com', 'Pablo', NULL, NULL, 'Fisica', NULL);
INSERT INTO encuestado VALUES ('Experto', 7, 'Carmona', 50, 'fbcarmona@gmail.com', 'Fernanda', NULL, NULL, 'Sistemas', NULL);
INSERT INTO encuestado VALUES ('Experto', 8, 'Gagliardi', 30, 'marizag@gmail.com', 'Mariza', NULL, NULL, 'Sistemas', NULL);
INSERT INTO encuestado VALUES ('Experto', 2, 'Yañez', 29, 'mpyañez@gmail.com', NULL, NULL, NULL, 'Software', 2);


--
-- TOC entry 2153 (class 0 OID 0)
-- Dependencies: 194
-- Name: encuestado_encuestadoid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('encuestado_encuestadoid_seq', 1, false);


--
-- TOC entry 2116 (class 0 OID 32831)
-- Dependencies: 181
-- Data for Name: formula; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2154 (class 0 OID 0)
-- Dependencies: 195
-- Name: formula_formulaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('formula_formulaid_seq', 1, false);


--
-- TOC entry 2117 (class 0 OID 32836)
-- Dependencies: 182
-- Data for Name: metrica; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO metrica VALUES (1, 'Efectividad Potencial', 1, NULL);
INSERT INTO metrica VALUES (2, 'Pertinencia y Rigurosidad', 2, NULL);
INSERT INTO metrica VALUES (3, 'Diseño Visual', 3, NULL);
INSERT INTO metrica VALUES (4, 'Reusabilidad', 4, NULL);
INSERT INTO metrica VALUES (5, 'Facilidad de Uso', 4, NULL);
INSERT INTO metrica VALUES (6, 'Facilidad de Acceso', 4, NULL);
INSERT INTO metrica VALUES (7, 'Completitud', 5, NULL);
INSERT INTO metrica VALUES (8, 'Precisión', 5, NULL);
INSERT INTO metrica VALUES (9, 'Motivación', 6, NULL);
INSERT INTO metrica VALUES (10, 'Efectividad', 6, NULL);
INSERT INTO metrica VALUES (11, 'Diseño Visual', 7, NULL);
INSERT INTO metrica VALUES (12, 'Disponibilidad', 8, NULL);
INSERT INTO metrica VALUES (13, 'Facilidad de Uso', 8, NULL);
INSERT INTO metrica VALUES (14, 'Precisión', 9, NULL);
INSERT INTO metrica VALUES (15, 'Relevancia', 10, NULL);


--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 196
-- Name: metrica_metricaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('metrica_metricaid_seq', 15, true);


--
-- TOC entry 2118 (class 0 OID 32841)
-- Dependencies: 183
-- Data for Name: objetoapredindizaje; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO objetoapredindizaje VALUES (1, 'Estrebou, César Armando', NULL, 'compuertas logicas', 'Español', 'OA compuertas lógicas', 'Compuertas lógicas digitales', 'La Plata', 2);
INSERT INTO objetoapredindizaje VALUES (2, 'Fernanda Carmona', NULL, 'Objeto de Aprendizaje', 'Español', '', 'Los Objetos de Aprendizaje en la Educación', 'Chilecito', 2);
INSERT INTO objetoapredindizaje VALUES (3, 'Alberto Riba', NULL, '', 'Español', '', 'Programación II', 'Chilecito', 1);


--
-- TOC entry 2107 (class 0 OID 32784)
-- Dependencies: 172
-- Data for Name: objetoaprendizaje_contribuyente; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2108 (class 0 OID 32787)
-- Dependencies: 173
-- Data for Name: objetoaprendizaje_descripcion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2109 (class 0 OID 32790)
-- Dependencies: 174
-- Data for Name: objetoaprendizaje_formatos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 197
-- Name: objetoaprendizaje_objetoaprendizajeid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('objetoaprendizaje_objetoaprendizajeid_seq', 3, true);


--
-- TOC entry 2110 (class 0 OID 32793)
-- Dependencies: 175
-- Data for Name: objetoaprendizaje_tema; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2111 (class 0 OID 32796)
-- Dependencies: 176
-- Data for Name: objetoaprendizaje_tipos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2119 (class 0 OID 32849)
-- Dependencies: 184
-- Data for Name: pregunta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO pregunta VALUES (1, '¿En qué nivel se logran identificar los objetos educativos que pretende cubrir el OA?', 1, 'E11');
INSERT INTO pregunta VALUES (2, '¿En qué nivel la estructura y contenido del OA apoyan el aprendizaje del tema?', 1, 'E12');
INSERT INTO pregunta VALUES (3, '¿En qué nivel el contenido presentado es claro, coherente, no discriminatorio, respeta derechos de autor, y se presenta sin sesgos u omisiones?', 2, 'E21');
INSERT INTO pregunta VALUES (4, '¿En qué nivel el OA se encuentra libre de errores ortográficos y gramaticales?', 2, 'E22');
INSERT INTO pregunta VALUES (5, 'La distribución y tamaño de elementos, la jerarquía visual, el diseño tipográfico y contraste de los colores ¿Es adecuado?', 3, 'E31');
INSERT INTO pregunta VALUES (6, 'La elección de los textos, imagenes, sonidos u otros elementos multimendia aportan a los objetivos de aprendizaje', 3, 'E32');
INSERT INTO pregunta VALUES (7, '¿Cuál es la posibilidad que ofrece el OA para que sea utilizado en varios escenarios educativos?', 4, 'E41');
INSERT INTO pregunta VALUES (8, '¿En qué grado el OA es auto contenido y no presenta dependencias?', 4, 'E42');
INSERT INTO pregunta VALUES (9, '¿Cuál es el nivel de claridad respecto a lo que debe hacer el usuario para utilizar el OA?', 5, 'E51');
INSERT INTO pregunta VALUES (10, 'Califique la relación entre la necesidad y la ayuda provista', 5, 'E52');
INSERT INTO pregunta VALUES (12, '¿En qué nivel no se requiere de software o dispositivos adicionales al momento de acceder al OA?', 6, 'E61');
INSERT INTO pregunta VALUES (13, '¿En qué medida el recurso funciona correctamente y es fácil para el usuario visualizarlo desde diferentes plataformas?', 6, 'E62');
INSERT INTO pregunta VALUES (14, '¿Qué tanto los metadatos que tiene el OA lo describen completamente?', 7, 'E71');
INSERT INTO pregunta VALUES (15, '¿En qué nvel los metadatos describen realmente el contenido encontrado?', 8, 'E81');
INSERT INTO pregunta VALUES (17, '¿En qué nivel el OA lo motivó a seguir consultando sobre el tema?', 9, 'U11');
INSERT INTO pregunta VALUES (18, '¿En qué nivel el contenido encontrado le permitió aprender sobre el tema?', 10, 'U21');
INSERT INTO pregunta VALUES (19, '¿En qué nivel el tamaño, color y distribución de los elementos que contiene el OA le parece adecuado?', 3, 'U31');
INSERT INTO pregunta VALUES (20, '¿En qué nivel los textos, imágenes, sonidos y otros elementos multimedia del OA le aportaron en el aprendizaje del tema?', 11, 'U32');
INSERT INTO pregunta VALUES (21, '¿Pudo acceder al contenido del OA? (NO=0 ; SI=5)', 12, 'U41');
INSERT INTO pregunta VALUES (22, 'Califique la facilidad y claridad a la hora de interactuar con el OA', 13, 'U51');
INSERT INTO pregunta VALUES (23, '¿En qué grado los metadatos describen el contenido que realmente encontró?', 14, 'U61');
INSERT INTO pregunta VALUES (24, '¿En qué nivel este OA fue importante para usted y estaba relacionado con lo que esperaba encontrar?', 15, 'U71');


--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 198
-- Name: pregunta_preguntaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pregunta_preguntaid_seq', 24, true);


--
-- TOC entry 2120 (class 0 OID 32854)
-- Dependencies: 185
-- Data for Name: reporteevaluacion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 199
-- Name: reporteevaluacion_reporteevaluacionid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reporteevaluacion_reporteevaluacionid_seq', 1, false);


--
-- TOC entry 2121 (class 0 OID 32859)
-- Dependencies: 186
-- Data for Name: repositorio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO repositorio VALUES (1, 'google', 'www.google.com');
INSERT INTO repositorio VALUES (2, 'SEDICI', 'www.sedici.com');


--
-- TOC entry 2159 (class 0 OID 0)
-- Dependencies: 200
-- Name: repositorio_repositorioid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('repositorio_repositorioid_seq', 2, true);


--
-- TOC entry 2122 (class 0 OID 32867)
-- Dependencies: 187
-- Data for Name: respuesta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2160 (class 0 OID 0)
-- Dependencies: 201
-- Name: respuesta_respuestaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('respuesta_respuestaid_seq', 1, false);


--
-- TOC entry 2123 (class 0 OID 32872)
-- Dependencies: 188
-- Data for Name: resultadoencuesta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO resultadoencuesta VALUES (2, 2, NULL);
INSERT INTO resultadoencuesta VALUES (3, 2, NULL);
INSERT INTO resultadoencuesta VALUES (4, 2, NULL);


--
-- TOC entry 2161 (class 0 OID 0)
-- Dependencies: 202
-- Name: resultadoencuesta_resultadoencuestaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('resultadoencuesta_resultadoencuestaid_seq', 1, false);


--
-- TOC entry 2140 (class 0 OID 40969)
-- Dependencies: 205
-- Data for Name: resultadoexperticiadimension; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2162 (class 0 OID 0)
-- Dependencies: 206
-- Name: resultadoexperticiadimension_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('resultadoexperticiadimension_seq', 1, false);


--
-- TOC entry 2124 (class 0 OID 32880)
-- Dependencies: 189
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rol VALUES (1, 'Administrador');
INSERT INTO rol VALUES (2, 'Encuestado');
INSERT INTO rol VALUES (3, 'Encuestador');


--
-- TOC entry 2163 (class 0 OID 0)
-- Dependencies: 203
-- Name: rol_rolid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rol_rolid_seq', 1, false);


--
-- TOC entry 2125 (class 0 OID 32885)
-- Dependencies: 190
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario VALUES (1, '123456', 'admin', 1, NULL);
INSERT INTO usuario VALUES (3, '789456', 'encuestador', 3, NULL);
INSERT INTO usuario VALUES (2, '456789', 'encuestado', 2, 2);


--
-- TOC entry 2164 (class 0 OID 0)
-- Dependencies: 204
-- Name: usuario_usuarioid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_usuarioid_seq', 1, false);


--
-- TOC entry 1944 (class 2606 OID 32806)
-- Name: capa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY capa
    ADD CONSTRAINT capa_pkey PRIMARY KEY (capaid);


--
-- TOC entry 1946 (class 2606 OID 32814)
-- Name: dimension_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dimension
    ADD CONSTRAINT dimension_pkey PRIMARY KEY (id);


--
-- TOC entry 1948 (class 2606 OID 32822)
-- Name: encuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY encuesta
    ADD CONSTRAINT encuesta_pkey PRIMARY KEY (id);


--
-- TOC entry 1950 (class 2606 OID 32830)
-- Name: encuestado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY encuestado
    ADD CONSTRAINT encuestado_pkey PRIMARY KEY (id);


--
-- TOC entry 1952 (class 2606 OID 32835)
-- Name: formula_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY formula
    ADD CONSTRAINT formula_pkey PRIMARY KEY (formulaid);


--
-- TOC entry 1954 (class 2606 OID 32840)
-- Name: metrica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY metrica
    ADD CONSTRAINT metrica_pkey PRIMARY KEY (metricaid);


--
-- TOC entry 1956 (class 2606 OID 32848)
-- Name: objetoapredindizaje_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY objetoapredindizaje
    ADD CONSTRAINT objetoapredindizaje_pkey PRIMARY KEY (objetoaprendizajeid);


--
-- TOC entry 1958 (class 2606 OID 32853)
-- Name: pregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT pregunta_pkey PRIMARY KEY (preguntaid);


--
-- TOC entry 1960 (class 2606 OID 32858)
-- Name: reporteevaluacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY reporteevaluacion
    ADD CONSTRAINT reporteevaluacion_pkey PRIMARY KEY (reporteevaluacionid);


--
-- TOC entry 1962 (class 2606 OID 32866)
-- Name: repositorio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY repositorio
    ADD CONSTRAINT repositorio_pkey PRIMARY KEY (repositorio);


--
-- TOC entry 1964 (class 2606 OID 32871)
-- Name: respuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY respuesta
    ADD CONSTRAINT respuesta_pkey PRIMARY KEY (respuestaid);


--
-- TOC entry 1966 (class 2606 OID 32879)
-- Name: resultadoencuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY resultadoencuesta
    ADD CONSTRAINT resultadoencuesta_pkey PRIMARY KEY (resultadoencuestaid);


--
-- TOC entry 1972 (class 2606 OID 40973)
-- Name: resultadoexperticiadimension_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY resultadoexperticiadimension
    ADD CONSTRAINT resultadoexperticiadimension_pkey PRIMARY KEY (resultadoexperticiaadimesionid);


--
-- TOC entry 1968 (class 2606 OID 32884)
-- Name: rol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (rolid);


--
-- TOC entry 1970 (class 2606 OID 32892)
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (usuarioid);


--
-- TOC entry 1995 (class 2606 OID 49161)
-- Name: encuestado_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT encuestado_fk FOREIGN KEY (encuestado) REFERENCES encuestado(id);


--
-- TOC entry 1984 (class 2606 OID 32948)
-- Name: fk_0f0b5d9862e640679164b43a447; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuesta
    ADD CONSTRAINT fk_0f0b5d9862e640679164b43a447 FOREIGN KEY (encuesta) REFERENCES reporteevaluacion(reporteevaluacionid);


--
-- TOC entry 1973 (class 2606 OID 32893)
-- Name: fk_1ffeaa737ec84c738ce341fd5bd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY enc_dim
    ADD CONSTRAINT fk_1ffeaa737ec84c738ce341fd5bd FOREIGN KEY (dimension_id) REFERENCES dimension(id);


--
-- TOC entry 1986 (class 2606 OID 32953)
-- Name: fk_4126bada60604d799332a6ef18c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY metrica
    ADD CONSTRAINT fk_4126bada60604d799332a6ef18c FOREIGN KEY (dimension_id) REFERENCES dimension(id);


--
-- TOC entry 1994 (class 2606 OID 32993)
-- Name: fk_48fc1f1bd37149c2a50cb020771; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fk_48fc1f1bd37149c2a50cb020771 FOREIGN KEY (rol_id) REFERENCES rol(rolid);


--
-- TOC entry 1987 (class 2606 OID 32958)
-- Name: fk_5dc49a6f3cfc4ea7bbd691fe42d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY metrica
    ADD CONSTRAINT fk_5dc49a6f3cfc4ea7bbd691fe42d FOREIGN KEY (idmetrica) REFERENCES formula(formulaid);


--
-- TOC entry 1981 (class 2606 OID 32933)
-- Name: fk_60dd9746e7e445a89fa854bc6d4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY objetoaprendizaje_tipos
    ADD CONSTRAINT fk_60dd9746e7e445a89fa854bc6d4 FOREIGN KEY (objetoaprendizaje_objetoaprendizajeid) REFERENCES objetoapredindizaje(objetoaprendizajeid);


--
-- TOC entry 1982 (class 2606 OID 32938)
-- Name: fk_6158bf3e45b64586b7c125d75c1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dimension
    ADD CONSTRAINT fk_6158bf3e45b64586b7c125d75c1 FOREIGN KEY (capa_id) REFERENCES capa(capaid);


--
-- TOC entry 1979 (class 2606 OID 32923)
-- Name: fk_7752b0354ef84918b48817883de; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY objetoaprendizaje_formatos
    ADD CONSTRAINT fk_7752b0354ef84918b48817883de FOREIGN KEY (objetoaprendizaje_objetoaprendizajeid) REFERENCES objetoapredindizaje(objetoaprendizajeid);


--
-- TOC entry 1997 (class 2606 OID 40979)
-- Name: fk_81da4b62c39d49078f4d8da67d6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultadoexperticiadimension
    ADD CONSTRAINT fk_81da4b62c39d49078f4d8da67d6 FOREIGN KEY (resultadoencuesta) REFERENCES resultadoencuesta(resultadoencuestaid);


--
-- TOC entry 1975 (class 2606 OID 32903)
-- Name: fk_864506f84fdd4821b18fd5d7c10; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY enc_encuestado
    ADD CONSTRAINT fk_864506f84fdd4821b18fd5d7c10 FOREIGN KEY (encuestado_id) REFERENCES encuestado(id);


--
-- TOC entry 1988 (class 2606 OID 32963)
-- Name: fk_8762687d07d2466fbf4bd7309c0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY objetoapredindizaje
    ADD CONSTRAINT fk_8762687d07d2466fbf4bd7309c0 FOREIGN KEY (repositorio_id) REFERENCES repositorio(repositorio);


--
-- TOC entry 1993 (class 2606 OID 32988)
-- Name: fk_8e3284c5cf744d06be8e5f43f1e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultadoencuesta
    ADD CONSTRAINT fk_8e3284c5cf744d06be8e5f43f1e FOREIGN KEY (reporte_id) REFERENCES reporteevaluacion(reporteevaluacionid);


--
-- TOC entry 1976 (class 2606 OID 32908)
-- Name: fk_9a99d92a96dc4c71982e5583805; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY enc_encuestado
    ADD CONSTRAINT fk_9a99d92a96dc4c71982e5583805 FOREIGN KEY (encuesta_id) REFERENCES encuesta(id);


--
-- TOC entry 1983 (class 2606 OID 32943)
-- Name: fk_a5f12c13fec94bb3b41f30e5f9d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuesta
    ADD CONSTRAINT fk_a5f12c13fec94bb3b41f30e5f9d FOREIGN KEY (objetoaprendizaje_id) REFERENCES objetoapredindizaje(objetoaprendizajeid);


--
-- TOC entry 1992 (class 2606 OID 32983)
-- Name: fk_ae18c500f9cf43f8b6b94eeeb77; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultadoencuesta
    ADD CONSTRAINT fk_ae18c500f9cf43f8b6b94eeeb77 FOREIGN KEY (encuestado_id) REFERENCES encuestado(id);


--
-- TOC entry 1977 (class 2606 OID 32913)
-- Name: fk_b372c33cd2cb4383b2b9102117e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY objetoaprendizaje_contribuyente
    ADD CONSTRAINT fk_b372c33cd2cb4383b2b9102117e FOREIGN KEY (objetoaprendizaje_objetoaprendizajeid) REFERENCES objetoapredindizaje(objetoaprendizajeid);


--
-- TOC entry 1991 (class 2606 OID 32978)
-- Name: fk_b5d63b6a6d83478ebf6f55546bb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY respuesta
    ADD CONSTRAINT fk_b5d63b6a6d83478ebf6f55546bb FOREIGN KEY (resultado_id) REFERENCES resultadoencuesta(resultadoencuestaid);


--
-- TOC entry 1996 (class 2606 OID 40974)
-- Name: fk_b744fa7963c9406ebb2938a3f93; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultadoexperticiadimension
    ADD CONSTRAINT fk_b744fa7963c9406ebb2938a3f93 FOREIGN KEY (dimension) REFERENCES dimension(id);


--
-- TOC entry 1980 (class 2606 OID 32928)
-- Name: fk_c3395f807af045d4a5bcc9d35b6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY objetoaprendizaje_tema
    ADD CONSTRAINT fk_c3395f807af045d4a5bcc9d35b6 FOREIGN KEY (objetoaprendizaje_objetoaprendizajeid) REFERENCES objetoapredindizaje(objetoaprendizajeid);


--
-- TOC entry 1974 (class 2606 OID 32898)
-- Name: fk_c67d294e63194f4f8b965f00fab; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY enc_dim
    ADD CONSTRAINT fk_c67d294e63194f4f8b965f00fab FOREIGN KEY (encuesta_id) REFERENCES encuesta(id);


--
-- TOC entry 1990 (class 2606 OID 32973)
-- Name: fk_c7ea96127d6c4158845a26bcafb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY respuesta
    ADD CONSTRAINT fk_c7ea96127d6c4158845a26bcafb FOREIGN KEY (pregunta_id) REFERENCES pregunta(preguntaid);


--
-- TOC entry 1989 (class 2606 OID 32968)
-- Name: fk_dea554b1272e4ba48345ecd6a1e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT fk_dea554b1272e4ba48345ecd6a1e FOREIGN KEY (metrica_id) REFERENCES metrica(metricaid);


--
-- TOC entry 1978 (class 2606 OID 32918)
-- Name: fk_f171f1d4b53241469a1e1147ea9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY objetoaprendizaje_descripcion
    ADD CONSTRAINT fk_f171f1d4b53241469a1e1147ea9 FOREIGN KEY (objetoaprendizaje_objetoaprendizajeid) REFERENCES objetoapredindizaje(objetoaprendizajeid);


--
-- TOC entry 1985 (class 2606 OID 49166)
-- Name: usuario_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuestado
    ADD CONSTRAINT usuario_fk FOREIGN KEY (usuario) REFERENCES usuario(usuarioid);


--
-- TOC entry 2148 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2020-01-14 19:19:16

--
-- PostgreSQL database dump complete
--

